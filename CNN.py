
import tensorflow as tf
import numpy as np
from sklearn.utils import shuffle
import sys
import os
import progressbar
#preprocessing

from osgeo import gdal
import numpy as np


ds = gdal.Open("D:\\Aprea\\Data\\THRS\\clip.tif")
image = ds.ReadAsArray()
ds = None



ds = gdal.Open("D:\\Aprea\\Data\\THRS\\gt_test_THRS.tif")
gt_test= ds.ReadAsArray()
ds = None

ds = gdal.Open("D:\\Aprea\\Data\\THRS\\gt_train_THRS.tif")
gt_train = ds.ReadAsArray()
ds = None

#Normalizzazione
image_n=[]
for j in range(len(image[:])):
    t_min = np.min(image[j,:,:])
    t_max = np.max(image[j,:,:])
    image_n.append((image[j,:,:]-t_min)/(t_max - t_min))

image = np.array(image_n)


def preprocessing(image,gt):

    patch_size = 25
    valid = int(patch_size/2)



    image = np.moveaxis(image, 0,2)
#Bordo
    gt[0:valid,:] = 0
    gt[len(gt)-valid:len(gt),:] = 0
    gt[:,0:valid]=0
    gt[:,len(gt)-valid : len(gt)] = 0
    sel = np.where(gt > 0)

    x, y = sel
    lst = image[sel]

    lista = []
    patch = []
    labels = []


    for n in range(len(x)):

        begin_i = x[n] - int(patch_size / 2)
        end_i = x[n] + int(patch_size / 2)+1
        begin_j = y[n] - int(patch_size / 2)
        end_j = y[n] + int(patch_size / 2)+1

        patch = image[begin_i: end_i, begin_j:end_j,:]
        lista.append(patch)
        labels.append(gt[x[n],y[n]])




    out= np.array(lista)
    labels= np.array(labels)


    #from tempfile import TemporaryFile

    #x_s = TemporaryFile()
    #np.save(x_s, out)

    #y_s = TemporaryFile()
    #np.save(y_s, labels)

    return(out,labels)



x_test, y_test = preprocessing(image, gt_test)

x_train, y_train = preprocessing(image, gt_train)


#architecture CNN

def CNN(x, nunits):
    conv1 = tf.layers.conv2d(inputs=x, filters= nunits/2, kernel_size=[7, 7], padding="valid", activation=tf.nn.relu)
    conv1 = tf.layers.batch_normalization(conv1)
    print(conv1.get_shape())
    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

    conv2 = tf.layers.conv2d(inputs=pool1, filters=nunits, kernel_size=[3, 3], padding="valid", activation=tf.nn.relu)
    conv2 = tf.layers.batch_normalization(conv2)

    print(conv2.get_shape())

    conv3 = tf.layers.conv2d(inputs=conv2, filters=nunits, kernel_size=[3, 3], padding="same", activation=tf.nn.relu)
    conv3 = tf.layers.batch_normalization(conv3)
    conv3 = tf.concat([conv2, conv3], 3)

    print(conv3.get_shape())

    conv4 = tf.layers.conv2d(inputs=conv3, filters=nunits, kernel_size=[1, 1], padding="valid", activation=tf.nn.relu)
    conv4 = tf.layers.batch_normalization(conv4)

    print(conv4.get_shape())

    conv4_shape = conv4.get_shape()

    cnn = tf.reduce_mean(conv4, [1, 2])

    print(cnn.get_shape())
    tensor_shape = cnn.get_shape()

    return(cnn, tensor_shape[1].value)


def getBatch(X, Y, i, batch_size):
    start_id = i*batch_size
    end_id = min( (i+1) * batch_size, X.shape[0])
    batch_x = X[start_id:end_id]
    batch_y = Y[start_id:end_id]
    return batch_x, batch_y




def getLabelFormat(Y):
    new_Y = []
    vals = np.unique(np.array(Y))
    for el in Y:
        t = np.zeros(len(vals))
        t[el] = 1.0
        new_Y.append(t)
    return np.array(new_Y)

"""
def data_augmentation(label_train, ts_train, vhsr_train):
    new_label_train = []
    new_ts_train = []
    new_vhsr_train = []
    for i in range(vhsr_train.shape[0]):
        img = vhsr_train[i]
        # ROTATE
        for j in range(1, 5):
            img_rotate = np.rot90(img, k=j, axes=(0, 1))
            # print img_rotate.shape
            new_label_train.append(label_train[i])
            new_ts_train.append(ts_train[i])
            new_vhsr_train.append(img_rotate)

        # FLIPPING
        for j in range(2):
            img_flip = np.rot90(img, j)
            new_label_train.append(label_train[i])
            new_ts_train.append(ts_train[i])
            new_vhsr_train.append(img_flip)

        # TRANSPOSE
        t_img = np.transpose(img, (1, 0, 2))
        new_label_train.append(label_train[i])
        new_ts_train.append(ts_train[i])
        new_vhsr_train.append(t_img)

    return np.array(new_label_train), np.array(new_ts_train), np.array(new_vhsr_train)
"""



#Model parameters
nunits = 512
batchsz = 64
hm_epochs = 400
n_levels_lstm = 1
droprate_rnn = 0.6


#Data INformation
n_timestamps = 23
n_dims = 16
patch_window = 25
n_channels = 4


nclasses = len(np.unique(y_train))



x_cnn = tf.placeholder("float", [None, patch_window, patch_window, n_channels], name="x_cnn")
y = tf.placeholder("float",[None,nclasses],name="y")

learning_rate = tf.placeholder(tf.float32, shape=[], name="learning_rate")
sess = tf.InteractiveSession()

out_cnn = CNN(x_cnn, nunits)

pred = tf.layers.dense(out_cnn,nclasses,activation=None)

#with tf.variable_scope("cost_prediction"):
cost = tf.math.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=pred))
prediction = tf.nn.softmax(pred)
valid_prediction = tf.math.argmax(prediction, 1, name="test_prediction")
correct = tf.math.equal(tf.math.argmax(prediction, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.dtypes.cast(correct, tf.float64))

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
logits = tf.layers.dense(out_cnn, nclasses, activation=None)

cost = tf.math.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=logits))
prediction = tf.nn.softmax(logits)

valid_prediction = tf.math.argmax(prediction, 1, name="test_prediction")
correct = tf.math.equal(tf.math.argmax(prediction, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.dtypes.cast(correct, tf.float64))

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)



#how calculate the iteratios?
iterations = int(x_train.shape[0] / batchsz)

if x_train.shape[0] % batchsz != 0:
    iterations+= 1

best_loss = sys.float_info.max

init = tf.global_variables_initializer()
sess.run(init)
saver = tf.train.Saver()

for e in range(hm_epochs):
    lossi = 0
    accS = 0

    x_train, y_train = shuffle(x_train, y_train, random_state= 0)
    print("shuffle DONE")


    with progressbar.ProgressBar(max_value=iterations) as bar:
        for ibatch in range(iterations):


            batch_cnn, batch_y = getBatch(x_train, y_train, ibatch, batchsz)

            acc, _, loss = sess.run([accuracy, optimizer, cost], {
                                                              x_cnn: batch_cnn,
                                                              y: batch_y,
                                                              dropout_rnn: 0.6,
                                                              learning_rate: 0.0002
                                                              })


            lossi += loss
            accS += acc
            del batch_y
            del batch_cnn

            if not ibatch % 30:
                bar.update(ibatch)

    print("Epoch:", e, "Train loss:", lossi / iterations, "| accuracy:", accS / iterations)
    c_loss = lossi / iterations

    if c_loss < best_loss:

        save_path = saver.save(sess, "/models_fusion/model_")
        print("Model saved in path: %s" % save_path)
        best_loss = c_loss
