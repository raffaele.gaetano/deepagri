import numpy as np
import tensorflow as tf
from tensorflow.contrib import rnn
import time
import random
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score


#Architettura RNN
def RnnAttention(x, nunits, nlayer, n_timetamps, dropout):
    x = tf.unstack(x, n_timetamps, 1)

    # NETWORK DEF
    # MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
    if nlayer > 1:
        cells = []
        for _ in range(nlayer):
            cell = rnn.GRUCell(nunits)
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

            cells.append(cell)
        cell = tf.contrib.rnn.MultiRNNCell(cells)
    # SIGNLE LAYER: single GRUCell, nunits hidden units each
    else:
        cell = rnn.GRUCell(nunits)
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

    outputs, _ = rnn.static_rnn(cell, x, dtype="float32")
    outputs = tf.stack(outputs, axis=1)

    # Trainable parameters
    attention_size = nunits  # int(nunits / 2)
    W_omega = tf.Variable(tf.random_normal([nunits, attention_size], stddev=0.1))
    b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
    u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

    v = tf.tanh(tf.tensordot(outputs, W_omega, axes=1) + b_omega)

    vu = tf.tensordot(v, u_omega, axes=1)  # (B,T) shape
    alphas = tf.nn.softmax(vu)  # (B,T) shape also

    output = tf.reduce_sum(outputs * tf.expand_dims(alphas, -1), 1)
    output = tf.reshape(output, [-1, nunits])

    return(output)



#Architettura CNN
def CNN(x, nunits):
    conv1 = tf.layers.conv2d(inputs=x, filters=nunits / 2, kernel_size=[7, 7], padding="valid", activation=tf.nn.relu)
    conv1 = tf.layers.batch_normalization(conv1)
    print(conv1.get_shape())


    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

    conv2 = tf.layers.conv2d(inputs=pool1, filters=nunits, kernel_size=[3, 3] ,padding="valid", activation=tf.nn.relu)
    conv2 = tf.layers.batch_normalization(conv2)

    print(conv2.get_shape())

    conv3 = tf.layers.conv2d(inputs=conv2,filters=nunits,kernel_size=[3, 3],padding="same",activation=tf.nn.relu)
    conv3 = tf.layers.batch_normalization(conv3)
    conv3 = tf.concat([conv2, conv3], 3)

    print(conv3.get_shape())

    conv4 = tf.layers.conv2d(inputs=conv3,filters=nunits,kernel_size=[1, 1],padding="valid",activation=tf.nn.relu)
    conv4 = tf.layers.batch_normalization(conv4)

    print(conv4.get_shape())
    conv4_shape = conv4.get_shape()
    cnn = tf.reduce_mean(conv4, [1, 2])

    print(cnn.get_shape())
    tensor_shape = cnn.get_shape()

    return (cnn, tensor_shape)


def checkTest(ts_data, vhsr_data, batchsz, label_valid):
    tot_pred = []

    iterations =int(ts_data.shape[0] / batchsz)

    if ts_data.shape[0] % batchsz != 0:
        iterations += 1

    for ibatch in range(iterations):
        batch_rnn_x = get_batch(ts_data, ibatch, batchsz)
        batch_cnn_x= get_batch(vhsr_data,ibatch, batchsz)
        batch_y = get_batch(labels_valid,ibatch,batchsz)


        pred_temp = sess.run(testPrediction, feed_dict={
                                                        x_rnn: batch_rnn_x,
                                                        dropout_rnn: 0.0,
                                                        is_training: False,
                                                        x_cnn: batch_cnn_x})

        del batch_rnn_x
        del batch_cnn_x
        del batch_y

        for el in pred_temp:
            tot_pred.append(el)

    tot_pred = np.array(tot_pred)
    return(accuracy_score(label_valid, tot_pred), np.array(tot_pred))



#creazione dei mini batch
def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return(batch)

#riorganizzazione dei dati
def getLabelFormat(Y):
    new_Y = []
    vals = np.unique(np.array(Y))
    for el in Y:
        t = np.zeros(len(vals))
        t[el] = 1.0
        new_Y.append(t)
    return np.array(new_Y)

def getRNNFormat(X, n_timestamps):
    new_X = np.reshape(X, (X.shape[0], n_timestamps, -1))
    print(new_X.shape)
    return (new_X)


def getPrediction(x_rnn, x_cnn, nunits, nlayer, nclasses, dropout, n_timetamps, is_training):
    features_learnt = None

    prediction = None
    features_learnt = None

    vec_rnn = RnnAttention(x_rnn, nunits, nlayer, n_timetamps, dropout)
    vec_rnn = tf.layers.dropout(vec_rnn, rate=dropout, training=is_training)

    vec_cnn, cnn_dim = CNN(x_cnn, 512)
    vec_cnn= tf.layers.dropout(vec_cnn, rate=dropout, training=is_training)

    features_learnt = tf.concat([vec_rnn, vec_cnn], axis=1, name="features")


    # Classifier1 #RNN Branch
    print("RNN Features:" )
    print(vec_rnn.get_shape())
    pred_rnn = tf.layers.dense(vec_rnn, nclasses, activation=None)

    # Classifier2 #CNN Branch
    print("CNN Features:")
    print(vec_cnn.get_shape())
    pred_cnn = tf.layers.dense(vec_cnn, nclasses, activation=None)

    # ClassifierFull
    print("FULL features_learnt:")
    print(features_learnt.get_shape())
    pred_full = tf.layers.dense(features_learnt, nclasses, activation=None)

    return(pred_rnn,pred_cnn,pred_full)


#caricamento dati
x_train_rnn =np.load('D:/Aprea\PyProjects\deepagri\RAF\GARD_train_FEAT_samples.npy')
labels_train =np.load('D:\Aprea\PyProjects\deepagri\RAF\GARD_train_sample_labels.npy')
x_train_cnn = np.load('D:\Aprea\PyProjects\deepagri\RAF\GARD_train_IMG_SPOT6_samples.npy')
x_valid_rnn =np.load('D:\Aprea\PyProjects\deepagri\RAF\GARD_valid_FEAT_samples.npy')
labels_valid =np.load('D:\Aprea\PyProjects\deepagri\RAF\GARD_valid_sample_labels.npy')
x_valid_cnn = np.load('D:\Aprea\PyProjects\deepagri\RAF\GARD_valid_IMG_SPOT6_samples.npy')




def percentuale(x,y,z):
    sel = np.random.rand(y.shape[0])
    selidx = np.where(sel < .1)
    x_p = x[selidx[0]]
    y_p = y[selidx[0]]
    z_p = z[selidx[0]]
    return(x_p,y_p,z_p)


#parametri
nclasses =len(np.unique(labels_train))

patch_window=25
n_channels=4
n_timestamps = 36

batch_size= 128
hm_epochs = 100

nunits=512
n_levels_lstm = 1
dout_rnn = 0.6

lrate = 0.0002




#Train
classes = labels_train - 1
rnn_data_train = getRNNFormat(x_train_rnn, n_timestamps)
cnn_data_train = x_train_cnn
train_y = getLabelFormat(classes)
print(np.unique(classes))
#Validation
rnn_data_valid = getRNNFormat(x_valid_rnn, n_timestamps)
cnn_data_valid = x_valid_cnn
n_bands = rnn_data_train.shape[2]

#rnn_data_train,cnn_data_train, train_y = percentuale(rnn_data_train, x_train_cnn, train_y)



x_rnn = tf.placeholder(tf.float32,shape=(None, n_timestamps, n_bands), name="x")
x_cnn = tf.placeholder("float", [None, patch_window, patch_window, n_channels], name="x_cnn")
y = tf.placeholder(tf.float32,shape=(None, nclasses), name='y')

learning_rate = tf.placeholder(tf.float32, shape=[], name="learning_rate")
dropout_rnn = tf.placeholder(tf.float32, shape=(), name="drop_rate")
is_training = tf.placeholder(tf.bool, shape=(), name="is_training")
sess = tf.InteractiveSession()

pred_c1, pred_c2, pred_full = getPrediction(x_rnn, x_cnn, nunits, n_levels_lstm, nclasses, dropout_rnn, n_timestamps,is_training)

loss_full = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=pred_full))
loss_c1 = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=pred_c1))
loss_c2 = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y, logits=pred_c2))

cost = loss_full + (0.3 * loss_c1) + (0.3 * loss_c2)
print('cost',cost)
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

pred_full = tf.nn.softmax(pred_full)

testPrediction = tf.argmax(pred_full, 1, name="prediction")

correct = tf.equal(tf.argmax(pred_full,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct, tf.float64))

tf.global_variables_initializer().run()

# Add ops to save and restore all the variables.
saver = tf.train.Saver()

iterations = int(x_train_cnn.shape[0] / batch_size)
print("iterations %d" % iterations)

if cnn_data_train.shape[0] % batch_size != 0:
    iterations += 1



for e in range(hm_epochs):
    lossi = 0
    accS = 0

    ts_train, vhsr_train, labels_train = shuffle(rnn_data_train, cnn_data_train, train_y, random_state=0)
    print("shuffle DONE")

    start = time.time()

    for ibatch in range(iterations):
        if not ibatch % 100:
            print("\tbatch: %d" % ibatch)

        batch_rnn_x =get_batch(ts_train, ibatch, batch_size)
        batch_cnn_x = get_batch(vhsr_train,ibatch, batch_size)
        batch_y = get_batch(labels_train, ibatch, batch_size)

        acc, _, loss = sess.run([accuracy, optimizer, cost], feed_dict={x_rnn: batch_rnn_x,
                                                                        x_cnn: batch_cnn_x,
                                                                        y: batch_y,
                                                                        is_training: True,
                                                                        dropout_rnn: dout_rnn,
                                                                        learning_rate: lrate})
        lossi += loss
        accS += acc

        del batch_rnn_x
        del batch_cnn_x
        del batch_y

    end = time.time()
    elapsed = end - start

    print("Epoch:", e, "Train loss:", lossi / iterations, "| accuracy:", accS / iterations, " | TIME: ", elapsed)

    c_loss = lossi / iterations
    save_path = saver.save(sess, "models/model_")
    print("Model saved in path: %s" % save_path)

    test_acc, pred_model = checkTest(rnn_data_valid, cnn_data_valid, 1024, labels_valid)
    print("test accuracy:", test_acc)