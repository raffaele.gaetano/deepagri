
from osgeo import gdal
import numpy as np
import sys

ds = gdal.Open(sys.argv[1])
#ds = gdal.Open("D:\\Aprea\\Data\\THRS\\clip.tif")
image = ds.ReadAsArray()
ds = None



ds = gdal.Open(sys.argv[3])
gt_test= ds.ReadAsArray()
ds = None

ds = gdal.Open(sys.argv[2])
gt_train = ds.ReadAsArray()
ds = None

#Normalizzazione
image_n=[]
for j in range(len(image[:])):
    t_min = np.min(image[j,:,:])
    t_max = np.max(image[j,:,:])
    image_n.append((image[j,:,:]-t_min)/(t_max - t_min))

image = np.array(image_n)


def preprocessing(image,gt):

    patch_size = 25
    valid = int(patch_size/2)



    image = np.moveaxis(image, 0,2)
#Bordo
    gt[0:valid,:] = 0
    gt[len(gt)-valid:len(gt),:] = 0
    gt[:,0:valid]=0
    gt[:,len(gt)-valid : len(gt)] = 0
    sel = np.where(gt > 0)

    x, y = sel
    lst = image[sel]

    lista = []
    patch = []
    labels = []


    for n in range(len(x)):

        begin_i = x[n] - int(patch_size / 2)
        end_i = x[n] + int(patch_size / 2)+1
        begin_j = y[n] - int(patch_size / 2)
        end_j = y[n] + int(patch_size / 2)+1

        patch = image[begin_i: end_i, begin_j:end_j,:]
        lista.append(patch)
        labels.append(gt[x[n],y[n]])




    out= np.array(lista)
    labels= np.array(labels)
    return(out,labels)



x_test, y_test= preprocessing(image, gt_test)
print(x_test.shape, y_test.shape)

np.save('x_test', x_test)
np.save('y_test', y_test)

x_train, y_train= preprocessing(image, gt_train)
print(x_train.shape, y_train.shape)

np.save('x_train', x_train)
np.save('y_train', y_train)

