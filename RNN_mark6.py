import sys
import tensorflow as tf
from tensorflow.contrib import rnn
from sklearn.utils import shuffle
from osgeo import gdal
import numpy as np
import glob
import os
import progressbar

def RnnAttention(x, nunits, nlayer, n_timetamps, dropout):
    x = tf.unstack(x, n_timetamps, 1)

    # NETWORK DEF
    # MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
    if nlayer > 1:
        cells = []
        for _ in range(nlayer):
            cell = rnn.GRUCell(nunits)
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

            cells.append(cell)
        cell = tf.contrib.rnn.MultiRNNCell(cells)
    # SIGNLE LAYER: single GRUCell, nunits hidden units each
    else:
        cell = rnn.GRUCell(nunits)
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

    outputs, _ = rnn.static_rnn(cell, x, dtype="float32")
    outputs = tf.stack(outputs, axis=1)

    # Trainable parameters
    attention_size = nunits  # int(nunits / 2)
    W_omega = tf.Variable(tf.random_normal([nunits, attention_size], stddev=0.1))
    b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
    u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

    v = tf.tanh(tf.tensordot(outputs, W_omega, axes=1) + b_omega)

    vu = tf.tensordot(v, u_omega, axes=1)  # (B,T) shape
    alphas = tf.nn.softmax(vu)  # (B,T) shape also

    output = tf.reduce_sum(outputs * tf.expand_dims(alphas, -1), 1)
    output = tf.reshape(output, [-1, nunits])

    return(output)


def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return(batch)



def getLabelFormat(Y):
    new_Y = []
    vals = np.unique(np.array(Y))
    for el in Y:
        t = np.zeros(len(vals))
        t[el] = 1.0
        new_Y.append(t)
    return np.array(new_Y)


def getRNNFormat(X, n_timestamps):
    new_X = np.reshape(X, (X.shape[0], n_timestamps, -1))
    print(new_X.shape)
    return new_X


#Model parameters
nunits = 1024
batchsz = 128
hm_epochs = 200
droprate_rnn = 0.6
n_levels_lstm = 1
learning_rate = 0.0002

X_Train = np.load('GARD_Train_feat.npy')
Y_Train = np.load('GARD_Train_labl.npy')

sel = np.random.rand(X_Train.shape[0])
selidx = np.where(sel<.1)
X_Train = X_Train[selidx[0]]
Y_Train = Y_Train[selidx[0]]





#print("shape X_Test:", X_Test.shape)
print("shape X_Train:", X_Train.shape)
#print("shape Y_Test:", Y_Test.shape)
print("shape Y_Train:", Y_Train.shape)

"""
np.save('x_test.npy',X_Test)
np.save('y_test.npy',Y_Test)
np.save('x_train.npy',X_Train)
np.save('y_train.npy',Y_Train)
"""

nclasses = len(np.unique(Y_Train))
n_timestamps = 36


#Train
classes = Y_Train - 1
rnn_data_train = getRNNFormat(X_Train, n_timestamps)
train_y = getLabelFormat(classes)
print(np.unique(classes))

n_bands = rnn_data_train.shape[2]

"""
#Test

classes_test = Y_Test - 1
rnn_data_test = getRNNFormat(X_Test, n_timestamps)
test_y = getLabelFormat(classes_test)
print(np.unique(classes_test))
"""

x = tf.placeholder(tf.float32,shape=(None, n_timestamps, n_bands), name="x")
y = tf.placeholder(tf.float32,shape=(None, nclasses), name='y')
#learning_rate = tf.placeholder(tf.float32, shape=(), name="learning_rate")
dropout_rnn = tf.placeholder(tf.float32, shape=(), name="drop_rate")

sess = tf.InteractiveSession()


out = RnnAttention(x,nunits, n_levels_lstm, n_timestamps, dropout_rnn )
pred = tf.layers.dense(out,nclasses,activation=None)

#with tf.variable_scope("cost_prediction"):
cost = tf.math.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=pred))
prediction = tf.nn.softmax(pred)
valid_prediction = tf.math.argmax(prediction, 1, name="test_prediction")
correct = tf.math.equal(tf.math.argmax(prediction, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.dtypes.cast(correct, tf.float64))

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)



iterations = int(rnn_data_train.shape[0] / batchsz)

if rnn_data_train.shape[0] % batchsz != 0:
    iterations+= 1

best_loss = sys.float_info.max

init = tf.global_variables_initializer()
sess.run(init)
saver = tf.train.Saver()

for e in range(hm_epochs):
    lossi = 0
    accS = 0

    rnn_data_train, train_y = shuffle(rnn_data_train, train_y, random_state = 0)
    print('shuffle DONE')

    with progressbar.ProgressBar(max_value=iterations) as bar:
        for ibatch in range(iterations):

            batch_rnn_x = get_batch(rnn_data_train, ibatch, batchsz)
            batch_y = get_batch(train_y, ibatch, batchsz)

            acc, _, loss = sess.run([accuracy, optimizer, cost], { x: batch_rnn_x,
                                                                   y : batch_y,
                                                                   dropout_rnn: 0.6
                                                                #learning_rate: 0.0002
                                                                 })
            lossi += loss
            accS += acc
            del batch_y
            del batch_rnn_x

            if not ibatch % 30:
                bar.update(ibatch)

    print("Epoch:", e, "Train loss:", lossi / iterations, "| accuracy:", accS / iterations)
    c_loss = lossi / iterations

    if c_loss < best_loss:
        if not os.path.exists("models"):
            os.makedirs("models")
        save_path = saver.save(sess, "models/model_" )
        print("Model saved in path: %s" % save_path)
        best_loss = c_loss