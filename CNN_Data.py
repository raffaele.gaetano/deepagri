
from osgeo import gdal
import numpy as np


ds = gdal.Open("D:\\Aprea\\Data\\THRS\\clip.tif")
image = ds.ReadAsArray()
ds = None



ds = gdal.Open("D:\\Aprea\\Data\\THRS\\gt_test_THRS.tif")
gt_test= ds.ReadAsArray()
ds = None

ds = gdal.Open("D:\\Aprea\\Data\\THRS\\gt_train_THRS.tif")
gt_train = ds.ReadAsArray()
ds = None

#Normalizzazione
image_n=[]
for j in range(len(image[:])):
    t_min = np.min(image[j,:,:])
    t_max = np.max(image[j,:,:])
    image_n.append((image[j,:,:]-t_min)/(t_max - t_min))

image = np.array(image_n)


def preprocessing(image,gt):

    patch_size = 25
    valid = int(patch_size/2)



    image = np.moveaxis(image, 0,2)
#Bordo
    gt[0:valid,:] = 0
    gt[len(gt)-valid:len(gt),:] = 0
    gt[:,0:valid]=0
    gt[:,len(gt)-valid : len(gt)] = 0
    sel = np.where(gt > 0)

    x, y = sel
    lst = image[sel]

    lista = []
    patch = []
    labels = []


    for n in range(len(x)):

        begin_i = x[n] - int(patch_size / 2)
        end_i = x[n] + int(patch_size / 2)+1
        begin_j = y[n] - int(patch_size / 2)
        end_j = y[n] + int(patch_size / 2)+1

        patch = image[begin_i: end_i, begin_j:end_j,:]
        lista.append(patch)
        labels.append(gt[x[n],y[n]])




    x_t= np.array(lista)
    y_t= np.array(labels)


    return(x_t,y_t)

x_train, y_train = preprocessing(image,gt_train)

x_test, y_test = preprocessing(image,gt_test)
#PROVA

import random

def data_augmentation(label_train, vhsr_train):
    new_label_train = []
    new_vhsr_train= []

    for i in range(vhsr_train.shape[0]):
        img= vhsr_train[i]

        # ROTATE
        new_vhsr_train.append(img)

        new_label_train.append(label_train[i])

        for j in range(1, 4):
            if (random.random() < 0.3):
                img_rotate= np.rot90(img, k=j, axes=(0, 1))

                new_label_train.append(label_train[i])
                new_vhsr_train.append(img_rotate)


        # FLIPPING
        for j in range(2):
            if (random.random() < 0.3):
                img_flip= np.rot90(img, j)

                new_label_train.append(label_train[i])
                # new_ts_train.append( ts_train[i])
                new_vhsr_train.append(img_flip)


        # TRANSPOSE
        if (random.random() < 0.3):
            t_img_1 = np.transpose(img, (1, 0, 2))

            new_label_train.append(label_train[i])
            new_vhsr_train.append(t_img_1)


    return np.array(new_label_train), np.array(new_vhsr_train)



y_train_new, x_train_new=data_augmentation(y_train, x_train)