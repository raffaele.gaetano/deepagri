import gdal
import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
import glob
import pandas


#Caricamento DATA
Data = {}
files = glob.glob("D:\\Aprea\\Data\\*S2_THEIA*.tif")

for fileName in files:
        ds = gdal.Open(fileName)
        arr_temp = ds.ReadAsArray()
        ds = None
        Data[fileName.split("/")[-1]] = arr_temp

print(Data.keys())

arr_MNDWI = Data['D:\\Aprea\\Data\\S2_THEIA_31TFJ_MNDWI.tif']
arr_NDVI = Data['D:\\Aprea\\Data\\S2_THEIA_31TFJ_NDVI.tif']


ds = gdal.Open('D:\\Aprea\\Data\\immagini_Rasterizzate\\gt_L0_S2.tif')
gt = ds.ReadAsArray()
ds = None

ds = gdal.Open('D:\\Aprea\\Data\\immagini_Rasterizzate\\gt_vector2.tif')
gt_vector = ds.ReadAsArray()
ds = None


#TIMEstamp
time_s =len(arr_NDVI[:])

#numero di classi
classes = np.unique(gt)
classes = sorted(classes)
if classes[0] == 0:
    c = classes[1:]


plt.figure(1)
plt.imshow(gt_vector)
plt.show()

plt.figure(2)
plt.imshow(gt)
plt.show()


#NORMALIZZAZIONE

#definire il valore max e min

def normalize(file):
    min_ = np.amin(file)
    max_ = np.amax(file)

    return(file.astype('float32') - min_)/(max_ - min_)



NDVI_n = normalize(arr_NDVI)
MNDWI_n = normalize(arr_MNDWI)



def readGT(gt_fileName, obj_fileName, NO_DATA_GT):

    ds = gdal.Open(gt_fileName)
    gt_matrix = ds.GetRasterBand(1).ReadAsArray()
    ds = None

    ds = gdal.Open(obj_fileName)
    obj_matrix = ds.GetRasterBand(1).ReadAsArray()
    ds = None


    TAB = []
    for i in range(gt_matrix.shape[0]):
        for j in range(gt_matrix.shape[1]):
            if gt_matrix[i][j] != NO_DATA_GT:
                TAB.append([i, j, gt_matrix[i][j], obj_matrix[i][j]])
    TAB = np.array(TAB)
    return TAB

directory = "D:\\Aprea\\Data\\immagini_Rasterizzate\\"
gt_fileName = directory + "gt_L0_S2.tif"
obj_fileName = directory + "gt_vector2.tif"
NO_DATA_GT = 0

TAB = readGT(gt_fileName, obj_fileName, NO_DATA_GT)
#Test & Training
#dati QGIs

ds = gdal.Open('D:\\Aprea\\Data\\Trening_Test\\07_May\\gt_Test30.tif')
gt_Test30 = ds.ReadAsArray()
ds = None

ds = gdal.Open('D:\\Aprea\\Data\\Trening_Test\\07_May\\gt_Train70.tif')
gt_Train70 = ds.ReadAsArray()
ds = None

plt.figure()
plt.imshow(gt_Test30)
plt.show()

plt.figure(2)
plt.imshow(gt_Train70)
plt.show()


def Test(c,TAB,p):
    p_Train =0.8
    classi = TAB[:,2]

    coord = np.where(classi == c)
    TAB = TAB[coord[0]]
    idx = np.unique(TAB[:,3])
    N = len(idx)
    idx_s = shuffle(idx, random_state = 0)
    idx_Test = idx_s[int(p*N):]


    valori_Test = {}
    for i in range(len(idx_Test)):
        coords_Test = np.where(idx_Test[i] == TAB[:,3])
        temp = TAB[coords_Test[0]]
        valori_Test[i] = temp

    Test_= None
    for k in valori_Test.keys():
        vec = valori_Test[k]
        temp = np.stack(vec)
        if Test_ is None:
            Test_ = temp
        else:
            Test_ = np.concatenate((Test_, temp), axis=0)

    return (Test_)


def Train(c,TAB,p):

    classi = TAB[:, 2]

    coord = np.where(classi == c)
    TAB = TAB[coord[0]]
    idx = np.unique(TAB[:, 3])
    N = len(idx)

    idx = np.unique(TAB[:, 3])
    idx_s = shuffle(idx, random_state=0)


    idx_Train = idx_s[: int(p* N)]
    valori_Train = {}
    for j in range(len(idx_Train)):
        coords_Train = np.where(idx_Train[j] == TAB[:,3])
        temp = TAB[coords_Train[0]]
        valori_Train[j] = temp

    Train_ = None
    for l in valori_Train.keys():
        vec = valori_Train[l]
        temp = np.stack(vec)
        if Train_ is None:
            Train_ = temp
        else:
            Train_ = np.concatenate((Train_, temp), axis=0)



    return( Train_)

p_Train = 0.8
Test = np.concatenate((Test(1, TAB,p_Train), Test(2,TAB,p_Train)), axis =0)

Train = np.concatenate((Train(1, TAB,p_Train),Train(2,TAB, p_Train)), axis =0)


def matrice (arr):
    # valori unici all'interno della matrice
    labels = np.unique(gt)
    labels = sorted(labels)

    hash_data = {}
# we start from 1 because the first element is 0 and it is the NO_DATA value
    for i in labels[1::]:
            # Restituisce un array di due dimensioni che contine le coordinate dei pixel
        coords = np.where(gt == i)

        temp_vec = arr[:, coords[0], coords[1]]

        temp_vec = np.transpose(temp_vec)
        hash_data[i] = temp_vec


    return(hash_data)

D_NDVI = matrice(NDVI_n)
D_MNDWI = matrice(MNDWI_n)

def concatena(indici):
    data = None
    for i in indici.keys():
        vec = indici[i]
        temp = np.stack(vec)
        if data is None:
            data = temp
        else:
            data = np.concatenate((data, temp), axis =0)


    return(data)


NDVI = concatena(D_NDVI)
MNDWI = concatena(D_MNDWI)




#Vettore dei descrittori
X = np.zeros((len(NDVI[:]),time_s*2))
for i in range(time_s):
    X[:, (2*i)] = NDVI[:, i]
    X[:, (2*i+1)] = MNDWI[:, i]



#Tensore 
tensor_1 = np.stack((D_NDVI[1], D_MNDWI[1]), axis=2)
tensor_2 = np.stack((D_NDVI[2], D_MNDWI[2]), axis=2)
tensor = np.concatenate((tensor_1, tensor_2), axis=0)
print(tensor.shape)


# Vettore Label
A = np.zeros((len(D_NDVI[1]), 1))
B = np.ones((len(D_NDVI[2]), 1))
Y = np.concatenate((A, B), axis=0)

#Label Test-Training
p_Train = 0.8
M = len(Y)
Y_s = shuffle(Y, random_state=0)
Y_Train = Y_s[int(p_Train*M):]
Y_Test = Y_s[: int(p_Train*M)]








