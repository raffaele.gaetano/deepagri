import numpy as np
from keras_custom_layers import BasicAttention
import keras
from keras.callbacks import CSVLogger
import keras.backend as K
import csv
import sys

#from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score,f1_score,confusion_matrix,precision_recall_fscore_support

#Input parameters
n_timestamps = 21
ts_size = 16
patch_size = 25
n_bands = 4

n_epochs = int(sys.argv[1])
attTanh = int(sys.argv[2]) == 1

# KOUMBIA
#Load training inputs
x_train_rnn = np.load('/home/deepcirad/data/KOUMBIA/process/KOUMBIA_train_TimeSeries_samples.npy')
#x_train_rnn = np.reshape(x_train_rnn, (x_train_rnn.shape[0], n_timestamps, -1))
x_train_cnn = np.load('/home/deepcirad/data/KOUMBIA/process/KOUMBIA_train_Koumbia_2016_PS_ORTHO_samples.npy')
labels_train = np.load('/home/deepcirad/data/KOUMBIA/process/KOUMBIA_train_sample_labels.npy')

#Load validation inputs
x_valid_rnn = np.load('/home/deepcirad/data/KOUMBIA/process/KOUMBIA_test_TimeSeries_samples.npy')
#x_valid_rnn = np.reshape(x_valid_rnn, (x_valid_rnn.shape[0], n_timestamps, -1))
x_valid_cnn = np.load('/home/deepcirad/data/KOUMBIA/process/KOUMBIA_test_Koumbia_2016_PS_ORTHO_samples.npy')
labels_valid = np.load('/home/deepcirad/data/KOUMBIA/process/KOUMBIA_test_sample_labels.npy')

#ModelPath
model_path = '/home/deepcirad/data/KOUMBIA/process/m3fusion_KOUMBIA_AttTanh_halved.h5'



'''
# GARD
#Load training inputs
x_train_rnn = np.load('/home/deepcirad/data/GARD/process/GARD_train_FEAT_samples.npy')
#x_train_rnn = np.reshape(x_train_rnn, (x_train_rnn.shape[0], n_timestamps, -1))
x_train_cnn = np.load('/home/deepcirad/data/GARD/process/GARD_train_IMG_SPOT6_samples.npy')
labels_train = np.load('/home/deepcirad/data/GARD/process/GARD_train_sample_labels.npy')

#Load validation inputs
x_valid_rnn = np.load('/home/deepcirad/data/GARD/process/GARD_valid_FEAT_samples.npy')
#x_valid_rnn = np.reshape(x_valid_rnn, (x_valid_rnn.shape[0], n_timestamps, -1))
x_valid_cnn = np.load('/home/deepcirad/data/GARD/process/GARD_valid_IMG_SPOT6_samples.npy')
labels_valid = np.load('/home/deepcirad/data/GARD/process/GARD_valid_sample_labels.npy')

#ModelPath
model_path = '/home/deepcirad/data/GARD/process/m3fusion_GARD_att_3.h5'
'''

weights_path = model_path.replace('.h5','_weights.h5')
log_path = model_path.replace('.h5','.log')
labeldec_path = model_path.replace('.h5','_labeldec.csv')

#Select random subset (for CPU DEBUG purposes)
'''
sel = np.where(np.random.random(labels_train.shape[0])<0.1)[0]
x_train_rnn = x_train_rnn[sel]
x_train_cnn = x_train_cnn[sel]
labels_train = labels_train[sel]
'''

# Prepare label vector (for train and validation)
n = labels_train.shape[0]
u = np.unique(labels_train)
label_encoder = {y:x for x,y in zip(range(len(u)),u)}
label_decoder = {x:y for x,y in zip(range(len(u)),u)}

with open(labeldec_path,'w') as dictfile:
    wrt = csv.writer(dictfile)
    for key,val in label_decoder.items():
        wrt.writerow([key,val])

n_classes = len(label_encoder.keys())
labels = np.zeros((n,n_classes))
for i in range(n):
    labels[i,label_encoder[labels_train[i]]] = 1.0

nv = labels_valid.shape[0]
labels_val = np.zeros((nv,n_classes))
for i in range(nv):
    labels_val[i,label_encoder[labels_valid[i]]] = 1.0

#RNN branch
#input_ts = keras.layers.Input(shape=(n_timestamps,ts_size),name='timeseries_input')
input_ts = keras.layers.Input(shape=(n_timestamps*ts_size,),name='timeseries_input')
resh = keras.layers.Reshape(input_shape=(n_timestamps*ts_size,),target_shape=(n_timestamps,ts_size),name='reshape_ts')(input_ts)
#rnn_out = keras.layers.GRU(512,return_sequences=True,name='gru_base')(input_ts)
rnn_out = keras.layers.GRU(256,return_sequences=True,name='gru_base')(resh)
#rnn_out = keras.layers.GRU(512,name='gru_base')(input_ts)
rnn_out = keras.layers.Dropout(rate=0.5,name='gru_dropout')(rnn_out)
rnn_out = BasicAttention(name='gru_attention', with_tanh=attTanh)(rnn_out)
rnn_aux = keras.layers.Dense(n_classes,activation='softmax',name='rnn_dense_layer_'+str(n_classes))(rnn_out)

#CNN branch
input_vhr = keras.layers.Input(shape=(patch_size,patch_size,n_bands),name='vhr_input')
cnn1 = keras.layers.Conv2D(128,[7,7],activation='relu',name='cnn_conv1')(input_vhr)
cnn1 = keras.layers.BatchNormalization(name='cnn_conv1_bn')(cnn1)
cnn1 = keras.layers.MaxPooling2D(strides=(2,2),name='cnn_conv1_pool')(cnn1)
cnn2 = keras.layers.Conv2D(256,[3,3],activation='relu',name='cnn_conv2')(cnn1)
cnn2 = keras.layers.BatchNormalization(name='cnn_conv2_bn')(cnn2)
cnn3 = keras.layers.Conv2D(256,[3,3],activation='relu',padding='same',name='cnn_conv3')(cnn2)
cnn3 = keras.layers.BatchNormalization(name='cnn_conv3_bn')(cnn3)
cnn4 = keras.layers.Concatenate(axis=3,name='cnn_inner_concat')([cnn2,cnn3])
cnn4 = keras.layers.Conv2D(256,[1,1],activation='relu',name='cnn_conv4')(cnn4)
cnn4 = keras.layers.BatchNormalization(name='cnn_conv4_bn')(cnn4)
cnn_out = keras.layers.GlobalAveragePooling2D(name='cnn_conv4_gpool')(cnn4)
cnn_aux = keras.layers.Dense(n_classes,activation='softmax',name='cnn_dense_layer_'+str(n_classes))(cnn_out)

#Merge branches
classifier = keras.layers.Concatenate(axis=-1,name='rnn_cnn_merge')([rnn_out,cnn_out])
classifier_out = keras.layers.Dense(n_classes,activation='softmax',name='full_dense_layer_'+str(n_classes))(classifier)
classifier_final = keras.layers.Lambda(lambda x:K.argmax(x,axis=1),name='output_class')(classifier_out)

#Compile model
model = keras.models.Model(inputs=[input_ts,input_vhr],outputs=[classifier_final,classifier_out,rnn_aux,cnn_aux])
opt = keras.optimizers.Adam(lr=0.0002)
model.compile(optimizer=opt,
              loss={'full_dense_layer_' + str(n_classes) : 'categorical_crossentropy',
                    'cnn_dense_layer_' + str(n_classes) : 'categorical_crossentropy',
                    'rnn_dense_layer_' + str(n_classes) : 'categorical_crossentropy'},
              loss_weights={'full_dense_layer_' + str(n_classes) : 1,
                    'cnn_dense_layer_' + str(n_classes) : 0.3,
                    'rnn_dense_layer_' + str(n_classes) : 0.3},
              metrics=['accuracy'])

#For transfer, put load_weights here with option by_name=True

#Create logger and launch training
log = CSVLogger(log_path, separator=',', append=False)
model.fit([x_train_rnn,x_train_cnn],[labels,labels,labels], epochs=7, batch_size=128, validation_data=([x_valid_rnn,x_valid_cnn],[labels_val,labels_val,labels_val]), callbacks=[log])
model.save(model_path)
model.save_weights(weights_path)

'''
model_cnn = keras.models.Model(inputs=[input_vhr],outputs=[cnn_aux])
model_cnn.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
#model_cnn.fit([x_train_cnn],[labels],epochs=10,batch_size=256,validation_data=([x_valid_cnn],[labels_val]))

model_rnn = keras.models.Model(inputs=[input_ts],outputs=[rnn_aux])
model_rnn.compile(optimizer='adam',loss='categorical_crossentropy',metrics=['accuracy'])
#model_rnn.fit([x_train_rnn],[labels],epochs=10,batch_size=256,validation_data=([x_valid_rnn],[labels_val]))

y_valid_pred = model.predict([x_valid_rnn,x_valid_cnn],batch_size=1024)
y_valid_pred_class = np.argmax(y_valid_pred,axis=1)
y_valid_pred_class = [label_decoder[y_valid_pred_class[i]] for i in range(len(y_valid_pred_class))]
#Calcolo indicatori con riferimento a label_valid
'''

#loaded_model = keras.models.load_model(model_path)
#loaded_model = keras.models.load_model(model_path,custom_objects={'BasicAttention':BasicAttention})
