from keras.layers import Layer
from keras.initializers import glorot_uniform
import keras.backend as K

class BasicAttention(Layer):
    """
    Simple attention operation.
    Thanks "Nigeljying".

    # Input shape
        3D tensor with shape: `(samples, steps, features)`.
    # Output shape
        2D tensor with shape: `(samples, features)`.

    Just put it on top of an RNN Layer (GRU/LSTM/SimpleRNN) with return_sequences=True.
    The dimensions are inferred based on the output shape of the RNN.

    Example:
        model.add(LSTM(64, return_sequences=True))
        model.add(AttentionWithContext())
    """

    def __init__(self, bias=True, with_tanh=False, **kwargs):

        self.supports_masking = True
        self.init = glorot_uniform()
        self.bias = bias
        self.with_tanh = with_tanh

        super(BasicAttention, self).__init__(**kwargs)

    def build(self, input_shape):
        assert len(input_shape) == 3

        self.W = self.add_weight((input_shape[-1], input_shape[-1],),
                                 initializer=self.init,
                                 name='{}_W'.format(self.name))
        if self.bias:
            self.b = self.add_weight((input_shape[-1],),
                                     initializer='zero',
                                     name='{}_b'.format(self.name))

        self.u = self.add_weight((input_shape[-1],),
                                 initializer=self.init,
                                 name='{}_u'.format(self.name))

        super(BasicAttention, self).build(input_shape)

    def compute_mask(self, input, input_mask=None):
        # do not pass the mask to the next layers
        return None

    def call(self, x, mask=None):
        uit = K.dot(x, self.W)


        if self.bias:
            uit += self.b

        uit = K.tanh(uit)

        ait = K.squeeze(K.dot(uit, K.expand_dims(self.u)),axis=-1)

        '''
        # Softmax (explicit)
        a = K.exp(ait)
        # apply mask after the exp. will be re-normalized next
        if mask is not None:
            # Cast the mask to floatX to avoid float64 upcasting in theano
            a *= K.cast(mask, K.floatx())
        # in some cases especially in the early stages of training the sum may be almost zero
        # and this results in NaN's. A workaround is to add a very small positive number ε to the sum.
        # a /= K.cast(K.sum(a, axis=1, keepdims=True), K.floatx()) --> WRONG
        a /= K.cast(K.sum(a, axis=1, keepdims=True) + K.epsilon(), K.floatx())
        '''

        if self.with_tanh:
            a = K.tanh(ait)
        else:
            a = K.softmax(ait)

        a = K.expand_dims(a)
        weighted_input = x * a

        return K.sum(weighted_input, axis=1)


    def compute_output_shape(self, input_shape):
        """
        Shape transformation logic so Keras can infer output shape
        """
        return (input_shape[0], input_shape[-1])