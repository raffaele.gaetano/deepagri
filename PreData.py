import gdal
import numpy as np
import matplotlib.pyplot as plt

ds = gdal.Open("D:\\Aprea\\Data\\Ref_New\\S2_THEIA_31TFJ_NDVI.tif")
arr_NDVI = ds.ReadAsArray()
ds = None

ds = gdal.Open("D:\\Aprea\\Data\\Ref_New\\S2_THEIA_31TFJ_MNDWI.tif")
arr_MNDWI = ds.ReadAsArray()
ds = None


ds = gdal.Open('D:\\Aprea\\Data\\Train_Test\\gt_L0_S2.tif')
gt = ds.ReadAsArray()
ds = None

print('Dim array è:', arr_NDVI.shape)
print('Dim gt è:',gt.shape)
print('Dim di arrMDWI:', arr_MNDWI.shape)


max=np.amax(gt)
min=np.amin(gt)
print(max)
print(min)

plt.figure(1)
plt.imshow(gt)
plt.show()


"""cerco i pixel che appartengono alla classe 1"""
pc1 = np.where(gt == 1)
print('Numero di pixel uguali a 1:',np.sum(gt == 1))
dim_c1 = np.sum(gt == 1)

pc2 = np.where(gt == 2)
print('Numero di pixel uguali a 2:',np.sum(gt == 2))
dim_c2 = np.sum(gt == 2)

dim = dim_c1 + dim_c2

#calcolo SOLO per verifica
pc0 = np.where(gt == 0)
print('Numero di pixel pari a 0:', np.sum(pc0))
dim_c0 = np.sum(gt == 0)

def matrice_trasposta(pc,arr,dim):
    elenco = []
 
    for i in range(36):
        lista = arr[i, :, :][pc]
        elenco.append(lista)
   
    matrice = np.zeros((36, dim))
    for j in range(36):
        matrice[j, :] = elenco[j]

    D=matrice.transpose()
    return D

D_1_NDVI = matrice_trasposta(pc=pc1, arr=arr_NDVI, dim=dim_c1)
print(D_1_NDVI.shape)
D_2_NDVI = matrice_trasposta(pc=pc2, arr=arr_NDVI, dim=dim_c2)
print(D_2_NDVI.shape)
D_1_MNDWI = matrice_trasposta(pc=pc1, arr=arr_MNDWI, dim=dim_c1)
print(D_1_MNDWI.shape)
D_2_MNDWI = matrice_trasposta(pc=pc2, arr=arr_MNDWI, dim=dim_c2)
print(D_2_MNDWI.shape)

def aumentDim(D,dim):
    matrice = np.zeros((dim, 72))
    matrice[:, 0:36] = D
    return(matrice)

D_1_NDVI_m = aumentDim(D_1_NDVI ,dim_c1)
D_1_MNDWI_m = aumentDim(D_1_MNDWI, dim_c1)
D_2_NDVI_m = aumentDim(D_2_NDVI, dim_c2)
D_2_MNDWI_m = aumentDim(D_2_MNDWI, dim_c2)



X = np.zeros((dim, 72))
X[0:dim_c1, 0:72:2] = D_1_NDVI_m[:, 0:36]
X[0:dim_c1, 1:72:2] = D_1_MNDWI_m[:, 0:36]

X[dim_c1:dim, 0:72:2] = D_2_NDVI_m[:, 0:36]
X[dim_c1:dim, 1:72:2] = D_2_MNDWI_m[:, 0:36]


"""vettore delle Classi"""
Y = np.zeros((dim, 1))
Y[0:dim_c1, :] = 0
Y[dim_c1:dim, :] = 1
print(Y.shape)

from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.3, random_state=42)
X_test = np.nan_to_num(X_test)
X_train = np.nan_to_num(X_train)


forest = RandomForestClassifier()
forest.fit(X_train, y_train.ravel())
y_predict = forest.predict(X_test)
accuracy_score(y_test, y_predict)
















"""
pc1 = np.where(gt == 1)
print(pc1)

pc2 = np.where(gt == 2)
pc1 = np.stack((pc1[0], pc1[1]), axis=1)
pc2 = np.stack((pc2[0], pc2[1]), axis=1)

PID = np.concatenate((pc1, pc2), axis=0)
"""


"""
#TEST TRAIN METODO 1
p_Train =0.8
OID = TAB[:, 3]
p_Test = 1-p_Train

idx = np.unique(OID)
N = len(idx)

idx_s = shuffle(idx, random_state = 0)

idx_Test = idx_s[int(p_Train*N):]
idx_Train = idx_s[: int(p_Train*N)]

valori_Test = {}
valori_Train = {}
for i in range(len(idx_Test)):
    coords_Test = np.where(idx_Test[i] == OID)
    temp = TAB[coords_Test[0]]
    valori_Test[i] = temp

for j in range(len(idx_Train)):
    coords_Train = np.where(idx_Train[j] == OID)
    temp = TAB[coords_Train[0]]
    valori_Train[j] = temp



Test = None
for k in valori_Test.keys():
    vec = valori_Test[k]
    temp = np.stack(vec)
    if Test is None:
        Test = temp
    else:
        Test = np.concatenate((Test, temp), axis=0)


Train = None
for l in valori_Train.keys():
    vec = valori_Train[l]
    temp = np.stack(vec)
    if Train is None:
        Train = temp
    else:
        Train = np.concatenate((Train, temp), axis=0)

"""