import sys
import tensorflow as tf
from tensorflow.contrib import rnn
from sklearn.utils import shuffle
import gdal
import numpy as np
import glob

def RnnAttention(x, nunits, nlayer, n_timetamps, dropout):
    x = tf.unstack(x, n_timetamps, 1)

    # NETWORK DEF
    # MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
    if nlayer > 1:
        cells = []
        for _ in range(nlayer):
            cell = rnn.GRUCell(nunits)
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

            cells.append(cell)
        cell = tf.contrib.rnn.MultiRNNCell(cells)
    # SIGNLE LAYER: single GRUCell, nunits hidden units each
    else:
        cell = rnn.GRUCell(nunits)
        cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

    outputs, _ = rnn.static_rnn(cell, x, dtype="float32")
    outputs = tf.stack(outputs, axis=1)

    # Trainable parameters
    attention_size = nunits  # int(nunits / 2)
    W_omega = tf.Variable(tf.random_normal([nunits, attention_size], stddev=0.1))
    b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
    u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

    v = tf.tanh(tf.tensordot(outputs, W_omega, axes=1) + b_omega)

    vu = tf.tensordot(v, u_omega, axes=1)  # (B,T) shape
    alphas = tf.nn.softmax(vu)  # (B,T) shape also

    output = tf.reduce_sum(outputs * tf.expand_dims(alphas, -1), 1)
    output = tf.reshape(output, [-1, nunits])

    return(output)


def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return(batch)

"""
def getLabelFormat(Y):
    #new_Y = []
    vals = np.unique(np.array(Y))
    new_Y = np.zeros((len(Y),len(vals)))
    for i in range(len(Y)):
        new_Y[Y[i]] = 1
    return new_Y
"""

def getLabelFormat(Y):
    new_Y = []
    vals = np.unique(np.array(Y))
    for el in Y:
        t = np.zeros(len(vals))
        t[el] = 1.0
        new_Y.append(t)
    return np.array(new_Y)


def getRNNFormat(X, n_timestamps):
    new_X = np.reshape(X, (X.shape[0], n_timestamps, -1))
    print(new_X.shape)
    return new_X


#Model parameters
nunits = 128
batchsz = 64
hm_epochs = 20
droprate_rnn = 0.6
n_levels_lstm = 1
learning_rate = 0.0002



"""
#Caricamento DATA
Data = {}
files = glob.glob("D:\\Aprea\\Data\\Ref_New\\*S2_THEIA*.tif")

for fileName in files:
        ds = gdal.Open(fileName)
        arr_temp = ds.ReadAsArray()
        ds = None
        Data[fileName.split("/")[-1]] = arr_temp

print(Data.keys())

arr_NDVI = Data[files[1]]
arr_NDVI = np.random.permutation(arr_NDVI)
arr_NDVI = arr_NDVI[:,:50,:]

arr_MNDWI = Data[files[0]]
arr_MNDWI= np.random.permutation(arr_MNDWI)
arr_MNDWI = arr_MNDWI[:,:50,:]
"""

def DATA_SET(arr_NDVI, arr_MNDWI):

    ds = gdal.Open('D:\\Aprea\\Data\\immagini_Rasterizzate\\gt_L0_S2.tif')
    gt = ds.ReadAsArray()

    ds = None



    #TIMEstamp
    time_s =len(arr_NDVI[:])

    #numero di classi
    classes = np.unique(gt)
    classes = sorted(classes)
    if classes[0] == 0:
        c = classes[1:]



#definire il valore max e min

    def normalize(file):
        min_ = np.nanmin(file)
        max_ = np.nanmax(file)

        return(file.astype('float32') - min_)/(max_ - min_)

    NDVI_n = normalize(arr_NDVI)
    MNDWI_n = normalize(arr_MNDWI)


#Test & Training
#dati QGIs

    ds = gdal.Open('D:\\Aprea\\Data\\Train_Test\\07_May\\gt_Test30.tif')
    gt_Test30 = ds.ReadAsArray()
    gt_Test30 = np.random.permutation(gt_Test30)
    gt_Test30 = gt_Test30[:50,:]
    print(gt_Test30.shape)
    ds = None




    ds = gdal.Open('D:\\Aprea\\Data\\Train_Test\\07_May\\gt_Train70.tif')
    gt_Train70 = ds.ReadAsArray()
    gt_Train70 = np.random.permutation(gt_Train70)
    gt_Train70 = gt_Train70[:50,:]
    print(gt_Train70.shape)
    ds = None



    def matrice (arr, gt):
    # valori unici all'interno della matrice
        labels = np.unique(gt)
        labels = sorted(labels)

        hash_data = {}
# we start from 1 because the first element is 0 and it is the NO_DATA value
        for i in labels[1::]:
            # Restituisce un array di due dimensioni che contine le coordinate dei pixel
            coords = np.where(gt == i)

            temp_vec = arr[:, coords[0], coords[1]]

            temp_vec = np.transpose(temp_vec)
            hash_data[i] = temp_vec


        return(hash_data)

    D_NDVI_Test = matrice(NDVI_n, gt_Test30)
    D_NDVI_Train = matrice(NDVI_n, gt_Train70)

    D_MNDWI_Test = matrice(MNDWI_n, gt_Test30)
    D_MNDWI_Train = matrice(MNDWI_n, gt_Train70)


    def concatena(indici):
        data = None
        for i in indici.keys():
            vec = indici[i]
            temp = np.stack(vec)
            if data is None:
                data = temp
            else:
                data = np.concatenate((data, temp), axis= 0)


        return(data)


    NDVI_Test = concatena(D_NDVI_Test)
    MNDWI_Test = concatena(D_MNDWI_Test)

    NDVI_Train = concatena(D_NDVI_Train)
    MNDWI_Train = concatena(D_MNDWI_Train)

#Matrice dei descrittori

    def matrice_Descrittori(arr_1, arr_2):
        X = np.zeros((len(arr_1[:]), time_s*2))
        for i in range(time_s):
            X[:, (2*i)] = arr_1[:, i]
            X[:, (2*i+1)] = arr_2[:, i]


        return(X)

    X_Test = matrice_Descrittori(NDVI_Test, MNDWI_Test)

    X_Train = matrice_Descrittori(NDVI_Train, MNDWI_Train)


    def vettore_label(gt_):

        idx = np.unique(gt_)
        idx = sorted(idx)

        label={}
        for i in idx[1::]:
            coord = np.where(gt_ == i)
            temp = gt_[coord]
            label[i] = temp

        data_ = None
        for l in label.keys():
            vec = label[l]
            temp = np.stack(vec)
            if data_ is None:
                data_ = temp
            else:
                data_ = np.concatenate((data_, temp), axis= 0)

            #data_ = np.reshape(data_, (len(data_),1))
        return(data_)

    Y_Test = vettore_label(gt_Test30)
    Y_Train = vettore_label(gt_Train70)

    X_Train = np.nan_to_num(X_Train)
    X_Test = np.nan_to_num(X_Test)


    return(X_Test,X_Train,Y_Test,Y_Train)


#X_Test, X_Train, Y_Test, Y_Train = DATA_SET(arr_NDVI, arr_MNDWI)

X_Train = np.load('x_train.npy')
Y_Train = np.load('y_train.npy')

sel = np.random.rand(X_Train.shape[0])
selidx = np.where(sel<.10)
X_Train = X_Train[selidx[0]]
Y_Train = Y_Train[selidx[0]]

"""
X_Test = np.load('x_test.npy')
Y_Test = np.load('y_test.npy')

sel = np.random.rand(X_Test.shape[0])
selidx = np.where(sel<.10)
X_Test = X_Test[selidx[0]]
Y_Test = Y_Test[selidx[0]]
"""



#print("shape X_Test:", X_Test.shape)
print("shape X_Train:", X_Train.shape)
#print("shape Y_Test:", Y_Test.shape)
print("shape Y_Train:", Y_Train.shape)

"""
np.save('x_test.npy',X_Test)
np.save('y_test.npy',Y_Test)
np.save('x_train.npy',X_Train)
np.save('y_train.npy',Y_Train)
"""

nclasses = len(np.unique(Y_Train))
n_timestamps = 36


#Train
classes = Y_Train - 1
rnn_data_train = getRNNFormat(X_Train, n_timestamps)
train_y = getLabelFormat(classes)
print(np.unique(classes))

n_bands = rnn_data_train.shape[2]

#Test
"""
classes_test = Y_Test - 1
rnn_data_test = getRNNFormat(X_Test, n_timestamps)
test_y = getLabelFormat(classes_test)
print(np.unique(classes_test))
"""

x = tf.placeholder(tf.float32,shape=(None, n_timestamps, n_bands), name="x")
y = tf.placeholder(tf.float32,shape=(None, nclasses), name='y')
#learning_rate = tf.placeholder(tf.float32, shape=(), name="learning_rate")
dropout_rnn = tf.placeholder(tf.float32, shape=(), name="drop_rate")

sess = tf.InteractiveSession()


out = RnnAttention(x,nunits, n_levels_lstm, n_timestamps, dropout_rnn )
pred = tf.layers.dense(out,nclasses,activation=None)

#with tf.variable_scope("cost_prediction"):
cost = tf.math.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=pred))
prediction = tf.nn.softmax(pred)
valid_prediction = tf.math.argmax(prediction, 1, name="test_prediction")
correct = tf.math.equal(tf.math.argmax(prediction, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.dtypes.cast(correct, tf.float64))

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)



iterations = int(rnn_data_train.shape[0] / batchsz)

if rnn_data_train.shape[0] % batchsz != 0:
    iterations+= 1

best_loss = sys.float_info.max

init = tf.global_variables_initializer()
sess.run(init)

for e in range(hm_epochs):
    lossi = 0
    accS = 0

    rnn_data_train, train_y = shuffle(rnn_data_train, train_y, random_state = 0)
    print('shuffle DONE')

    for ibatch in range(iterations):

        if not ibatch % 100:
            print('Iteration #',ibatch)
        batch_rnn_x = get_batch(rnn_data_train, ibatch, batchsz)
        batch_y = get_batch(train_y, ibatch, batchsz)

        acc, _, loss = sess.run([accuracy, optimizer, cost], { x: batch_rnn_x,
                                                                y : batch_y,
                                                                dropout_rnn: 0.6
                                                                #learning_rate: 0.0002

                                                                        })
        lossi += loss
        accS += acc
        del batch_y
        del batch_rnn_x

    print("Epoch:", e, "Train loss:", lossi / iterations, "| accuracy:", accS / iterations)
    c_loss = lossi / iterations
