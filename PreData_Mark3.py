import gdal
import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
import glob

#Caricamento DATA
Data = {}
files = glob.glob("D:\\Aprea\\Data\\Ref_New\\*S2_THEIA*.tif")

for fileName in files:
        ds = gdal.Open(fileName)
        arr_temp = ds.ReadAsArray()
        ds = None
        Data[fileName.split("/")[-1]] = arr_temp

print(Data.keys())

arr_NDVI = Data[files[1]]
arr_MNDWI = Data[files[0]]



ds = gdal.Open('D:\\Aprea\\Data\\immagini_Rasterizzate\\gt_L0_S2.tif')
gt = ds.ReadAsArray()
ds = None

plt.figure(1)
plt.imshow(gt)
plt.title('gt')
plt.show()


#TIMEstamp
time_s =len(arr_NDVI[:])

#numero di classi
classes = np.unique(gt)
classes = sorted(classes)
if classes[0] == 0:
    c = classes[1:]



#definire il valore max e min

def normalize(file):
    min_ = np.nanmin(file)
    max_ = np.nanmax(file)

    return(file.astype('float32') - min_)/(max_ - min_)

NDVI_n = normalize(arr_NDVI)
MNDWI_n = normalize(arr_MNDWI)


#Test & Training
#dati QGIs

ds = gdal.Open('D:\\Aprea\\Data\\Train_Test\\07_May\\gt_Test30.tif')
gt_Test30 = ds.ReadAsArray()
ds = None

plt.figure()
plt.imshow(gt_Test30)
plt.title('gt_Test30')
plt.show()


ds = gdal.Open('D:\\Aprea\\Data\\Train_Test\\07_May\\gt_Train70.tif')
gt_Train70 = ds.ReadAsArray()
ds = None

plt.figure()
plt.imshow(gt_Train70)
plt.title('gt_Train70')
plt.show()


def matrice (arr, gt):
    # valori unici all'interno della matrice
    labels = np.unique(gt)
    labels = sorted(labels)

    hash_data = {}
# we start from 1 because the first element is 0 and it is the NO_DATA value
    for i in labels[1::]:
            # Restituisce un array di due dimensioni che contine le coordinate dei pixel
        coords = np.where(gt == i)

        temp_vec = arr[:, coords[0], coords[1]]

        temp_vec = np.transpose(temp_vec)
        hash_data[i] = temp_vec


    return(hash_data)

D_NDVI_Test = matrice(NDVI_n,gt_Test30)
D_NDVI_Train = matrice(NDVI_n,gt_Train70)

D_MNDWI_Test = matrice(MNDWI_n,gt_Test30)
D_MNDWI_Train = matrice(MNDWI_n,gt_Train70)


def concatena(indici):
    data = None
    for i in indici.keys():
        vec = indici[i]
        temp = np.stack(vec)
        if data is None:
            data = temp
        else:
            data = np.concatenate((data, temp), axis =0)


    return(data)


NDVI_Test = concatena(D_NDVI_Test)
MNDWI_Test = concatena(D_MNDWI_Test)

NDVI_Train = concatena(D_NDVI_Train)
MNDWI_Train = concatena(D_MNDWI_Train)

#Matrice dei descrittori

def matrice_Descrittori(arr_1,arr_2):
    X = np.zeros((len(arr_1[:]),time_s*2))
    for i in range(time_s):
        X[:, (2*i)] = arr_1[:, i]
        X[:, (2*i+1)] = arr_2[:, i]


    return(X)

X_Test = matrice_Descrittori(NDVI_Test, MNDWI_Test)
X_Train = matrice_Descrittori(NDVI_Train, MNDWI_Train)


def vettore_label(gt_):
    idx = np.unique(gt_)
    idx = sorted(idx)

    label={}
    for i in idx[1::]:
        coord = np.where(gt_ == i)
        temp = gt_[coord]
        label[i] = temp

    data_ = None
    for l in label.keys():
        vec = label[l]
        temp = np.stack(vec)
        if data_ is None:
            data_ = temp
        else:
            data_ = np.concatenate((data_,temp), axis = 0)

            #data_ = np.reshape(data_, (len(data_),1))
    return(data_)

Y_Test = vettore_label(gt_Test30)
Y_Train = vettore_label(gt_Train70)




#Random Forest

from sklearn.preprocessing import StandardScaler

sc = StandardScaler()
X_Train = sc.fit_transform(X_Train)
X_Test = sc.transform(X_Test)
#coverte Nan in 0
X_Train = np.nan_to_num(X_Train)
X_Test = np.nan_to_num(X_Test)

from sklearn.ensemble import RandomForestClassifier


model = RandomForestClassifier()
model.fit(X_Train, Y_Train.ravel())
y_predict = model.predict(X_Test)



from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

print(confusion_matrix(Y_Test, y_predict))
print(classification_report(Y_Test, y_predict))
print(accuracy_score(Y_Test, y_predict))