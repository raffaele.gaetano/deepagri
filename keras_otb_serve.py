import keras
import tensorflow as tf
from keras_custom_layers import BasicAttention
import os
from tricks import *

model_file = '/home/deepcirad/data/GARD/process/m3fusion_GARD_att_3.h5'
tf_model_dir = os.path.splitext(model_file)[0]
tf_model_file = os.path.join(tf_model_dir,'model.ckpt')
if not os.path.exists(tf_model_dir):
    os.mkdir(tf_model_dir)

k_model = keras.models.load_model(model_file,custom_objects={"BasicAttention":BasicAttention})
sess = keras.backend.get_session()

CreateSavedModel(sess,['timeseries_input:0','vhr_input:0'],['output_class/ArgMax:0'],tf_model_dir)

"""
saver = tf.train.Saver()
saver.save(sess,tf_model_file)

n_timestamps = 21
ts_size = 16
patch_size = 25
n_bands = 4
"""
# to be done after input format check!