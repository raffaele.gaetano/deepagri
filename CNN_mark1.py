import tensorflow as tf
import time
import random
from sklearn.utils import shuffle
import numpy as np

#Architettura rete
def CNN(x, nunits):
    conv1 = tf.layers.conv2d(inputs=x, filters=nunits / 2, kernel_size=[7, 7], padding="valid", activation=tf.nn.relu)
    conv1 = tf.layers.batch_normalization(conv1)
    print(conv1.get_shape())


    pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)

    conv2 = tf.layers.conv2d(inputs=pool1, filters=nunits, kernel_size=[3, 3] ,padding="valid", activation=tf.nn.relu)
    conv2 = tf.layers.batch_normalization(conv2)

    print(conv2.get_shape())

    conv3 = tf.layers.conv2d(inputs=conv2,filters=nunits,kernel_size=[3, 3],padding="same",activation=tf.nn.relu)
    conv3 = tf.layers.batch_normalization(conv3)
    conv3 = tf.concat([conv2, conv3], 3)

    print(conv3.get_shape())

    conv4 = tf.layers.conv2d(inputs=conv3,filters=nunits,kernel_size=[1, 1],padding="valid",activation=tf.nn.relu)
    conv4 = tf.layers.batch_normalization(conv4)

    print(conv4.get_shape())
    conv4_shape = conv4.get_shape()
    cnn = tf.reduce_mean(conv4, [1, 2])

    print(cnn.get_shape())
    tensor_shape = cnn.get_shape()

    return (cnn)


def getLabelFormat(Y):
	new_Y = []
	vals = np.unique(np.array(Y))
	for el in Y:
		t = np.zeros(len(vals))
		t[el] = 1.0
		new_Y.append(t)
	return np.array(new_Y)


def get_batch(array, i, batch_size):
    start_id = i*batch_size
    end_id = min((i+1) * batch_size, array.shape[0])
    batch = array[start_id:end_id]
    return(batch)






def data_augmentation(label_train, vhsr_train):
    new_label_train = []
    new_vhsr_train= []

    for i in range(vhsr_train.shape[0]):
        img= vhsr_train[i]

        # ROTATE
        new_vhsr_train.append(img)

        new_label_train.append(label_train[i])

        for j in range(1, 4):
            if (random.random() < 0.3):
                img_rotate= np.rot90(img, k=j, axes=(0, 1))

                new_label_train.append(label_train[i])
                new_vhsr_train.append(img_rotate)


        # FLIPPING
        for j in range(2):
            if (random.random() < 0.3):
                img_flip= np.rot90(img, j)

                new_label_train.append(label_train[i])
                # new_ts_train.append( ts_train[i])
                new_vhsr_train.append(img_flip)


        # TRANSPOSE
        if (random.random() < 0.3):
            t_img_1 = np.transpose(img, (1, 0, 2))

            new_label_train.append(label_train[i])
            new_vhsr_train.append(t_img_1)


    return np.array(new_label_train), np.array(new_vhsr_train)


#caricamento dati
"""
x_train = np.load('x_train.npy')
print(x_train.shape)
y_train = np.load('y_train.npy')
print(y_train.shape)
x_test = np.load('x_test.npy')
y_test = np.load('y_test.npy')
"""

y_train=np.load('GARD_2016_BD_v0.1_train_sample_labels.npy')

x_train= np.load('IMG_SPOT6_samples.npy')


sel = np.random.rand(x_train.shape[0])
selidx = np.where(sel<.1)
x_train = x_train[selidx[0]]
y_train = y_train[selidx[0]]

#parametri
nclasses =len(np.unique(y_train))
batch_size= 128
patch_window=25
n_channels=4
nunits=524
learning_rate = 0.0002
hm_epochs = 10

label_train, vhsr_train = data_augmentation(y_train, x_train)

x_cnn = tf.placeholder("float", [None, patch_window, patch_window, n_channels], name="x_cnn")
y = tf.placeholder("float",[None,nclasses],name="y")

learning_rate = tf.placeholder(tf.float32, shape=[], name="learning_rate")
dropout = tf.placeholder(tf.float32, shape=(), name="drop_rate")
sess = tf.InteractiveSession()


out_cnn = CNN(x_cnn, nunits)
pred_cnn= tf.layers.dense(out_cnn,nclasses,activation=None)

cost = tf.math.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=y, logits=pred_cnn))
prediction = tf.nn.softmax(pred_cnn)
valid_prediction = tf.math.argmax(prediction, 1, name="test_prediction")

correct = tf.math.equal(tf.math.argmax(prediction, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.dtypes.cast(correct, tf.float64))

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
tf.global_variables_initializer().run()


#organizzazione labels
classes_train = label_train - 1
train_y = getLabelFormat(classes_train)

#classes_test = y_test - 1
#test_y = getLabelFormat(classes_test)

iterations = int(vhsr_train.shape[0] / batch_size)
print("iterations %d" % iterations)

if vhsr_train.shape[0] % batch_size != 0:
    iterations += 1



for e in range(hm_epochs):
    lossi = 0
    accS = 0

    vhsr_train, train_y = shuffle(vhsr_train, train_y, random_state=0)
    print("shuffle DONE")

    print(vhsr_train.shape)



    start = time.time()
    # for ibatch in range(20):
    for ibatch in range(iterations):
        if not ibatch % 100:
            print("\tbatch: %d" % ibatch)


        batch_cnn_x = get_batch(vhsr_train,ibatch, batch_size)
        batch_y = get_batch(train_y, ibatch, batch_size)

        acc, _, loss = sess.run([accuracy, optimizer, cost], feed_dict={x_cnn: batch_cnn_x,

                                                                        y: batch_y,

                                                                        dropout: 0.4,
                                                                        learning_rate: 0.0002})
        lossi += loss
        accS += acc

        del(batch_cnn_x)
        del(batch_y)

    end = time.time()
    elapsed = end - start

    print("Epoch:", e, "Train loss:", lossi / iterations, "| accuracy:", accS / iterations, " | TIME: ", elapsed)

    c_loss = lossi / iterations


