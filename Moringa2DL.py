import os
import sys
from osgeo import gdal,ogr,osr
import subprocess
import glob
import numpy as np
import xml.etree.ElementTree as ET
import random,string
import otbApplication as otb
import csv
import time
from math import floor

import keras.models
from keras_custom_layers import BasicAttention

def generateAnnotatedDataset(fld, sampling, cl_field, sname='AnnotatedSet', ofld='.', do_norm=True):
    imfeat = sorted(glob.glob(fld + os.sep + '*_FEAT.tif'))

    cmd = ['otbcli_Rasterization', '-in', sampling, '-im', imfeat[0], '-mode', 'attribute', '-mode.attribute.field', cl_field, '-out', ofld + os.sep + sname + '_gt.tif', 'uint16']
    subprocess.call(cmd)

    ds = gdal.Open(sname + '_gt.tif')
    gt = ds.ReadAsArray()
    ds = None

    sel = np.where(gt > 0)
    lst = []
    ts_size = None

    for fn in imfeat:
        ds = gdal.Open(fn)
        if ts_size is None:
            ts_size = ds.RasterCount
        img = ds.ReadAsArray()
        ds = None
        lst.append(np.moveaxis(img, 0, 2)[sel])

    feat = np.concatenate(tuple(lst), axis=1)
    feat[np.where(np.isnan(feat))] = 0

    if do_norm is True:
        for i in range(ts_size):
            t_min, t_max = np.min(feat[:, i::ts_size]), np.max(feat[:, i::ts_size])
            feat[:, i::ts_size] = (feat[:, i::ts_size] - t_min) / (t_max - t_min)
    labl = gt[sel]

    np.save(ofld + os.sep + sname + '_feat.npy', feat)
    np.save(ofld + os.sep + sname + '_labl.npy', labl)

    return feat, labl

def generateMultiSourceAnnotatedDataset(source_list, patch_sizes, reference, cl_field, image_reference, sampling_strategy='smallest', sname='AnnotatedSet', ofld='.', do_norm=True, add_sampling_params=None):

    lbl = True

    # Subprocess version
    stat_file = ofld + os.sep + os.path.splitext(os.path.basename(reference))[0] + '_polystat.xml'
    cmd = ['otbcli_PolygonClassStatistics', '-in', image_reference, '-vec', reference, '-field', cl_field, '-out',
           stat_file]
    subprocess.call(cmd)

    # Subprocess version
    samples_file = ofld + os.sep + os.path.splitext(os.path.basename(reference))[0] + '_sample_loc.shp'
    cmd = ['otbcli_SampleSelection', '-in', image_reference, '-vec', reference, '-field', cl_field, '-out',
           samples_file, '-instats', stat_file, '-strategy', sampling_strategy]
    if add_sampling_params is not None:
        cmd += add_sampling_params
    subprocess.call(cmd)

    for src,psz in zip(source_list,patch_sizes):
        mns = []
        sds = []

        cmd = ['otbcli_PatchesExtraction']
        n_timestamps = 1
        if os.path.isdir(src):
            imfeat = sorted(glob.glob(src + os.sep + '*_FEAT.tif'))
            cmd += ['-source1.il'] + imfeat
            n_timestamps = len(imfeat)
            mns,sds = computeGlobalMeanStd(imfeat)
        else:
            cmd += ['-source1.il', src]
            mns, sds = computeGlobalMeanStd(src)

        cmd += ['-source1.patchsizex', str(psz), '-source1.patchsizey', str(psz), '-vec', samples_file, '-field', cl_field]
        if lbl:
            labels_file = ofld + os.sep + sname + '_sample_labels.tif'
            cmd += ['-outlabels', labels_file, 'uint16']
            lbl = False

        patch_file = ofld + os.sep + sname + '_' + os.path.splitext(os.path.basename(src))[0] + '_samples.tif'
        cmd += ['-source1.out', patch_file, 'float']
        subprocess.call(cmd)

        ds = gdal.Open(patch_file)
        arr = ds.ReadAsArray()
        ds = None

        if n_timestamps == 1:

            if do_norm:
                # band-by-band
                '''
                for b in range(arr.shape[0]):
                    b_min, b_max = np.min(arr[b]), np.max(arr[b])
                    arr[b] = (arr[b] - b_min) / (b_max - b_min)
                '''
                # all bands
                '''
                b_min, b_max = np.min(arr), np.max(arr)
                arr = (arr - b_min) / (b_max - b_min)
                '''
                # global standardization
                for b in range(arr.shape[0]):
                    arr[b] = (arr[b] - mns[b]) / sds[b]

            arr = np.reshape(np.moveaxis(arr, 0, -1), (int(arr.shape[1] / arr.shape[2]), arr.shape[2], arr.shape[2], arr.shape[0]))
            np.save(patch_file.replace('.tif','.npy'),arr)

        else:

            ts_size = int(arr.shape[0] / n_timestamps)
            arr = np.moveaxis(arr, 0, -1).squeeze()
            arr = np.nan_to_num(arr)

            if do_norm:
                # min-max bandwise scaling
                '''
                for i in range(ts_size):
                    t_min, t_max = np.min(arr[:, i::ts_size]), np.max(arr[:, i::ts_size])
                    arr[:, i::ts_size] = (arr[:, i::ts_size] - t_min) / (t_max - t_min)
                '''
                # bandwise standardization
                for i in range(ts_size):
                    arr[:, i::ts_size] = (arr[:, i::ts_size] - mns[i]) / sds[i]
            np.save(patch_file.replace('.tif', '.npy'), arr)

        ds = gdal.Open(labels_file)
        arr = ds.ReadAsArray().squeeze()
        ds = None
        np.save(labels_file.replace('.tif', '.npy'), arr)

    return

def computeGlobalMeanStd(sources):

    timeseries = True
    if not isinstance(sources,list):
        sources = [sources]
        timeseries = False

    ds = gdal.Open(sources[0])
    ndv = ds.GetRasterBand(1).GetNoDataValue()
    ds = None
    print('Using no-data value : ' + str(ndv))

    nm = os.path.splitext(sources[0])[0] + '.stats.xml'
    if not os.path.exists(nm):
        cmd = ['otbcli_ComputeImagesStatistics','-il'] + sources + ['-out', nm]
        if ndv is not None:
            cmd += ['-bv', str(ndv)]
        subprocess.call(cmd)

    vals = []
    root = ET.parse(nm).getroot()
    for item in root.findall('Statistic'):
        for child in item:
            vals.append(float(child.attrib['value']))
    mns = np.array(vals[:int(len(vals)/2)])
    sds = np.array(vals[int(len(vals)/2):])

    """
    if timeseries:
        tmns = []
        tsds = []
        for i in range(len(sources)):
            tmns.append(np.mean(mns[i::B]))
        tmns = np.array(tmns)
        for i in range(len(sources)):
            tsds.append(np.sqrt((np.sum(np.power(sds[i::B],2)) + np.sum(np.power(mns[i::B]-tmns[i],2)))/B))
        mns = np.array(tmns)
        sds = np.array(sds)
    """

    return mns,sds

def generateOneStreamPositions(ref, lines, out):
    ds = gdal.Open(ref)
    gt = ds.GetGeoTransform()
    proj = ds.GetProjection()
    C = ds.RasterXSize
    ds = None

    srs = osr.SpatialReference()
    srs.ImportFromWkt(proj)

    coords = []
    for line in range(lines[0],lines[1]+1):
        line_coord = gt[3] + (line+0.5)*gt[5]
        for i in range(C):
            coords.append((gt[0] + (i+0.5)*gt[1],line_coord))

    drv = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists(out):
        drv.DeleteDataSource(out)
    ds = drv.CreateDataSource(out)
    ly = ds.CreateLayer(os.path.basename(os.path.splitext(out)[0]), srs = srs, geom_type = ogr.wkbPoint)
    idfield = ogr.FieldDefn('ID',ogr.OFTInteger)
    ly.CreateField(idfield)

    id = 0
    for c in coords:
        p = ogr.Geometry(ogr.wkbPoint)
        p.AddPoint(c[0],c[1])
        f = ogr.Feature(ly.GetLayerDefn())
        f.SetGeometry(p)
        f.SetField('ID',id)
        ly.CreateFeature(f)
        f = None
        id += 1

    ds = None
    coords = None

    return

def generateOneStreamTestSet(source_list, patch_sizes, image_reference, lines, tmp_dir='/tmp', do_norm = True):
    tmpname = ''.join(random.choice(string.ascii_lowercase) for i in range(16))
    ptf = os.path.join(tmp_dir,tmpname + '.shp')
    generateOneStreamPositions(image_reference,lines,ptf)

    out = []

    for src,psz in zip(source_list,patch_sizes):
        mns = []
        sds = []

        #patchext = otb.Registry.CreateApplication('PatchesExtraction')
        patchext = ['otbcli_PatchesExtraction']

        n_timestamps = 1

        if os.path.isdir(src):
            imfeat = sorted(glob.glob(src + os.sep + '*_FEAT.tif'))
            #patchext.SetParameterStringList('source1.il',imfeat)
            patchext += ['-source1.il'] + imfeat
            n_timestamps = len(imfeat)
            mns,sds = computeGlobalMeanStd(imfeat)
        else:
            #patchext.SetParameterStringList('source1.il', [src])
            patchext += ['-source1.il', src]
            mns, sds = computeGlobalMeanStd(src)

        """
        patchext.SetParameterInt('source1.patchsizex', int(psz))
        patchext.SetParameterInt('source1.patchsizey', int(psz))
        patchext.SetParameterString('vec', ptf)
        patchext.UpdateParameters()
        patchext.SetParameterString('field', 'ID')

        patchext.Execute()
        arr = patchext.GetVectorImageAsNumpyArray('source1.out')
        """
        of = os.path.join(tmp_dir,'pimg.tif')
        patchext += ['-source1.patchsizex', str(int(psz)), '-source1.patchsizey', str(int(psz)), '-vec', ptf, '-field', 'ID', '-source1.out', of]
        subprocess.call(patchext)
        ds = gdal.Open(of)
        arr = np.moveaxis(ds.ReadAsArray(),0,-1)
        ds = None
        os.remove(of)

        if n_timestamps == 1:
            # global standardization
            for b in range(arr.shape[2]):
                arr[:,:,b] = (arr[:,:,b] - mns[b]) / sds[b]
            arr = np.reshape(arr, (int(arr.shape[0] / arr.shape[1]), arr.shape[1], arr.shape[1], arr.shape[2]))
        else:
            ts_size = int(arr.shape[2] / n_timestamps)
            arr = arr.squeeze()
            arr = np.nan_to_num(arr)
            # bandwise standardization
            for i in range(ts_size):
                arr[:, i::ts_size] = (arr[:, i::ts_size] - mns[i]) / sds[i]
            arr = np.reshape(arr, (arr.shape[0], n_timestamps, -1))

        out.append(arr)

    drv = ogr.GetDriverByName('ESRI Shapefile')
    drv.DeleteDataSource(ptf)

    return out

def kerasModelServe(source_list, patch_sizes, image_reference, model_file, output_map, stream_line_size=10):
    # Read image reference info
    ds = gdal.Open(image_reference)
    Xs,Ys = ds.RasterXSize, ds.RasterYSize
    gt = ds.GetGeoTransform()
    proj = ds.GetProjection()
    ds = None
    # Prepare output image
    drv = gdal.GetDriverByName('GTiff')
    ds = drv.Create(output_map, Xs, Ys, 1, gdal.GDT_UInt16)
    ds.SetGeoTransform(gt)
    ds.SetProjection(proj)
    ds = None
    # Read label dictionary
    df = model_file.replace('.h5','_labeldec.csv')
    label_decoder = {}
    with open(df,'r') as dfh:
        rdr = csv.reader(dfh)
        for row in rdr:
            label_decoder[int(row[0])] = int(row[1])
    # Load model
    mdl = keras.models.load_model(model_file, custom_objects={'BasicAttention':BasicAttention})

    # Sequential streaming
    for l in range(0,Ys,stream_line_size):
        strm = (l,l+stream_line_size-1)
        if l+stream_line_size > Ys:
            strm = (l, Ys - 1)

        arr = generateOneStreamTestSet(source_list, patch_sizes, image_reference, strm)

        Y = mdl.predict(arr,batch_size=256)

        Yc = np.argmax(Y[0],axis=1)
        Yc = np.array([label_decoder[Yc[i]] for i in range(len(Yc))])
        Yc = np.reshape(Yc,(int(Yc.shape[0]/Xs),-1)).astype(np.uint16)

        ds = gdal.Open(output_map,1)
        bnd = ds.GetRasterBand(1)
        bnd.WriteArray(Yc,yoff=l)
        Yc = None
        Y = None
        bnd = None
        ds = None
        arr = None

    return 0

def normalizeDataset(src):

    if os.path.isdir(src):
        imfeat = sorted(glob.glob(src + os.sep + '*_FEAT.tif'))
        n_timestamps = len(imfeat)
        mns, sds = computeGlobalMeanStd(imfeat)
        for im in imfeat:
            cmd = ['otbcli_BandMathX','-il',im,'-exp']
            expr = '{' + ';'.join(['(im1b%s-%s)/%s' % (str(i), str(m), str(s)) for i, m, s in
                                   zip(range(1, mns.shape[0] + 1), mns, sds)]) + '}'
            cmd.append(expr)
            cmd += ['-out', im.replace('.tif','_norm.tif')]
            subprocess.call(cmd)
    else:
        mns, sds = computeGlobalMeanStd(src)
        cmd = ['otbcli_BandMathX', '-il', src, '-exp']
        expr = '{' + ';'.join(['(im1b%s-%s)/%s' % (str(i), str(m), str(s)) for i, m, s in zip(range(1,mns.shape[0]+1), mns, sds)]) + '}'
        cmd.append(expr)
        cmd += ['-out', src.replace(os.path.splitext(src)[-1], '_norm.tif')]
        subprocess.call(cmd)

    return

if __name__ == '__main__':
    timeseries = sys.argv[1]
    vhsr = sys.argv[2]
    p_ts = int(sys.argv[3])
    p_vhr = int(sys.argv[4])
    im_ref = sys.argv[5]
    mod_file = sys.argv[6]
    out_map = sys.argv[7]

    kerasModelServe([timeseries,vhsr],[p_ts,p_vhr],im_ref,mod_file,out_map)
