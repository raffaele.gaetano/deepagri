import sys
import os
import numpy as np
import math
from operator import itemgetter, attrgetter, methodcaller
import tensorflow as tf
from tensorflow.contrib import rnn
import random
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.utils import shuffle
from sklearn.metrics import confusion_matrix


def RnnAttention(x, nunits, nlayer, n_timetamps, dropout):

	x = tf.unstack(x,n_timetamps,1)

	#NETWORK DEF
	#MORE THEN ONE LAYER: list of LSTMcell,nunits hidden units each, for each layer
	if nlayer>1:
		cells=[]
		for _ in range(nlayer):
			cell = rnn.GRUCell(nunits)
			cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

			cells.append(cell)
		cell = tf.contrib.rnn.MultiRNNCell(cells)
    #SIGNLE LAYER: single GRUCell, nunits hidden units each
	else:
		cell = rnn.GRUCell(nunits)
		cell = tf.nn.rnn_cell.DropoutWrapper(cell, output_keep_prob=dropout)

	outputs,_=rnn.static_rnn(cell, x, dtype="float32")
	outputs = tf.stack(outputs, axis=1)

	# Trainable parameters
	attention_size = nunits #int(nunits / 2)
	W_omega = tf.Variable(tf.random_normal([nunits, attention_size], stddev=0.1))
	b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
	u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

	v = tf.tanh(tf.tensordot(outputs, W_omega, axes=1) + b_omega)

	vu = tf.tensordot(v, u_omega, axes=1)   # (B,T) shape
	alphas = tf.nn.softmax(vu)              # (B,T) shape also

	output = tf.reduce_sum(outputs * tf.expand_dims(alphas, -1), 1)
	output = tf.reshape(output, [-1, nunits])

	return output
	


#
def extractFeatures(ts_data, vhsr_data):
	batchsz = 1024
	iterations = ts_data.shape[0] / batchsz

	if ts_data.shape[0] % batchsz != 0:
	    iterations+=1
	
	
	label_test = np.zeros(len(ts_data))
	test_y = np.zeros(len(ts_data))
	
	features = None
	
	for ibatch in range(iterations):
		batch_rnn_x, batch_y = getBatch(ts_data, label_test, ibatch, batchsz)
		bach_rnn_x_b = batch_rnn_x[::-1]
		
		
		batch_cnn_x, batch_y = getBatch(vhsr_data, test_y, ibatch, batchsz)
		
		#batch_cnn_x = np.swapaxes(batch_cnn_x,1,3)
		partial_features = sess.run(features_learnt,feed_dict={x_rnn:batch_rnn_x, 
																#x_rnn_b:bach_rnn_x_b, 
																x_cnn:batch_cnn_x})
		if features is None:
			features = partial_features
		else:
			features = np.vstack((features, partial_features))
	
	print ( features.shape)
	return features

def checkTest(ts_data, vhsr_data, batchsz, label_test):
	tot_pred = []
#	gt_test = []
	iterations = ts_data.shape[0] / batchsz

	if ts_data.shape[0] % batchsz != 0:
	    iterations+=1

	for ibatch in range(iterations):
		batch_rnn_x, _ = getBatch(ts_data, label_test, ibatch, batchsz)
		#reversing the sequence ordering to feed the backward rnn
		#batch_rnn_x_b = batch_rnn_x[::-1]
		
		batch_cnn_x, batch_y = getBatch(vhsr_data, test_y, ibatch, batchsz)

		#batch_cnn_x = np.swapaxes(batch_cnn_x,1,3)
		pred_temp = sess.run(testPrediction,feed_dict={x_rnn:batch_rnn_x,
														 dropout_rnn:1.0,
		 												 #x_rnn_b:batch_rnn_x_b,
														 x_cnn:batch_cnn_x})
		
		del batch_rnn_x
		del batch_cnn_x
		del batch_y
		
		for el in pred_temp:
			tot_pred.append( el )
	
	tot_pred = np.array(tot_pred)
	print (tot_pred[np.where(tot_pred < 0)])
	print (np.unique(label_test))
	print (np.bincount(np.array(tot_pred)))
	print (np.bincount(np.array(label_test)))


	print ("TEST F-Measure: %f" % f1_score(label_test, tot_pred, average='weighted'))
	print (f1_score(label_test, tot_pred, average=None))
	print ("TEST Accuracy: %f" % accuracy_score(label_test, tot_pred))
	print ("==========================================")
	print (confusion_matrix(label_test, tot_pred))
	print ("==========================================")
	sys.stdout.flush()
	return accuracy_score(label_test, tot_pred)







	


def CNN(x, nunits):
	conv1 = tf.layers.conv2d(
	      inputs=x,
	      filters=nunits/2, #256
	      kernel_size=[7, 7],
	      padding="valid",
	      activation=tf.nn.relu)
	
	conv1 = tf.layers.batch_normalization(conv1)
	print (conv1.get_shape())
#	conv1_shape = conv1.get_shape()
	
	pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)
	
	conv2 = tf.layers.conv2d(
	      inputs=pool1,
	      filters=nunits,
	      kernel_size=[3, 3],
	      padding="valid",
	      activation=tf.nn.relu)
	
	conv2 = tf.layers.batch_normalization(conv2)
	
	print (conv2.get_shape())
	
	conv3 = tf.layers.conv2d(
	      inputs=conv2,
	      filters=nunits,
	      kernel_size=[3, 3],
	      padding="same",
		  #padding="valid",
	      activation=tf.nn.relu)
	
	conv3 = tf.layers.batch_normalization(conv3)
	
	conv3 = tf.concat([conv2,conv3],3)
	
	print (conv3.get_shape())
	
	conv4 = tf.layers.conv2d(
	      inputs=conv3,
	      filters=nunits,
	      kernel_size=[1, 1],
	      padding="valid",
	      activation=tf.nn.relu)
	
	conv4 = tf.layers.batch_normalization(conv4)
	
	print (conv4.get_shape())
	
	conv4_shape = conv4.get_shape()
	
	cnn = tf.reduce_mean(conv4, [1,2])
		
	print(cnn.get_shape())
	tensor_shape = cnn.get_shape()
	#cnn = tf.nn.tanh(cnn)
	return cnn, tensor_shape[1].value
	

def getBatch(X, Y, i, batch_size):
    start_id = i*batch_size
    end_id = min( (i+1) * batch_size, X.shape[0])
    batch_x = X[start_id:end_id]
    batch_y = Y[start_id:end_id]
    return batch_x, batch_y

def getLabelFormat(Y):
	new_Y = []
	vals = np.unique(np.array(Y))
	for el in Y:
		t = np.zeros(len(vals))
		t[el] = 1.0
		new_Y.append(t)
	return np.array(new_Y)
	
	'''	
	sorted(vals)
	hash_val = {}
	for el in vals:
		hash_val[el] = len(hash_val.keys())
	new_Y = []
	'''
	
def getRNNFormat(X, n_timestamps):
    #print X.shape
    new_X = []
    for row in X:
        new_X.append( np.split(row, n_timestamps) )
    return np.array(new_X)


#def getPrediction(x_rnn, x_rnn_b, x_cnn, nunits, nlayer, nclasses, choice):
def getPrediction(x_rnn, x_cnn, nunits, nlayer, nclasses, dropout, n_timetamps):
	features_learnt = None

	prediction = None
	features_learnt = None
	

	vec_rnn = RnnAttention(x_rnn, nunits, nlayer, n_timetamps, dropout)
	
	vec_cnn, cnn_dim = CNN(x_cnn, 512)		
	
	
	features_learnt=tf.concat([vec_rnn,vec_cnn],axis=1, name="features")
	first_dim = cnn_dim + nunits
	
	
	#Classifier1 #RNN Branch
	print( "RNN Features:")
	print( vec_rnn.get_shape())
	pred_c1 = tf.layers.dense(vec_rnn, nclasses, activation=None)
		
	#Classifier2 #CNN Branch
	print( "CNN Features:")
	print (vec_cnn.get_shape())
	pred_c2 = tf.layers.dense(vec_cnn, nclasses, activation=None)

	#ClassifierFull
	print ("FULL features_learnt:")
	print (features_learnt.get_shape())
	pred_full = tf.layers.dense(features_learnt, nclasses, activation=None)	
		
	return pred_c1, pred_c2, pred_full, features_learnt
	
	#return prediction, features_learnt
	
def data_augmentation( label_train, ts_train, vhsr_train ):
	new_label_train = []
	new_ts_train = []
	new_vhsr_train = []
	for i in range(vhsr_train.shape[0]):
		img = vhsr_train[i]
		#ROTATE
		for j in range(1,5):
			img_rotate = np.rot90(img, k=j, axes=(0, 1))
			#print img_rotate.shape
			new_label_train.append( label_train[i] )
			new_ts_train.append( ts_train[i])
			new_vhsr_train.append( img_rotate )
			
		
		#FLIPPING
		for j in range(2):
			img_flip = np.rot90(img, j)
			new_label_train.append( label_train[i] )
			new_ts_train.append( ts_train[i])
			new_vhsr_train.append( img_flip )

	 	#TRANSPOSE
		t_img = np.transpose(img, (1,0,2))
		new_label_train.append( label_train[i] )
		new_ts_train.append( ts_train[i])
		new_vhsr_train.append( t_img )

	return np.array(new_label_train), np.array(new_ts_train), np.array(new_vhsr_train)

def addNDVI(img):
	red = img[:,:,0]
	nir = img[:,:,-1]
	ndvi = (nir - red) / (nir + red)
	ndvi = np.reshape(ndvi, (ndvi.shape[0], ndvi.shape[1], 1))
	return np.concatenate((img,ndvi), axis=2)
	

def computeNDVI(vhsr_train):
	new_data = []
	for img in vhsr_train:
		new_data.append( addNDVI(img) )
	return np.array(new_data)



#Model parameters
nunits = 1024
batchsz = 64
hm_epochs = 400
n_levels_lstm = 1
droprate_rnn = 0.6


#Data INformation
n_timestamps = 23
n_dims = 16
patch_window = 25
n_channels = 5

ts_train = np.load(sys.argv[1])
vhsr_train = np.load(sys.argv[2])


print (vhsr_train.shape)
label_train = np.load(sys.argv[3])

nclasses = len(np.unique(label_train))

print (np.bincount(label_train))

ts_test = np.load(sys.argv[4])
vhsr_test = np.load(sys.argv[5])

label_test = np.load(sys.argv[6])
split_numb = int(sys.argv[7])

#outputFileName = sys.argv[8]

print ("before Data augmentation")
print( ts_train.shape)
print (vhsr_train.shape)
print (label_train.shape)

#label_train, ts_train, vhsr_train = data_augmentation( label_train, ts_train, vhsr_train )

print ("After Data augmentation")
print (ts_train.shape)
print (vhsr_train.shape)
print (label_train.shape)



#exit()
print ("ciao")



x_rnn = tf.placeholder("float",[None,n_timestamps,n_dims],name="x_rnn")
x_cnn = tf.placeholder("float",[None,patch_window,patch_window,n_channels],name="x_cnn")

y = tf.placeholder("float",[None,nclasses],name="y")

learning_rate = tf.placeholder(tf.float32, shape=[], name="learning_rate")

dropout_rnn = tf.placeholder(tf.float32, shape=(), name="drop_rate")

sess = tf.InteractiveSession()


#prediction, features_learnt = getPrediction(x_rnn, x_rnn_b, x_cnn, nunits, n_levels_lstm, nclasses, choice)
pred_c1, pred_c2, pred_full, features_learnt = getPrediction(x_rnn, x_cnn, nunits, n_levels_lstm, nclasses, dropout_rnn, n_timestamps)

#test_prediction, features = getPrediction(x_rnn, x_cnn, nunits, n_levels_lstm, nclasses, choice, dropout, reuse=True, is_training=False)


loss_full = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=pred_full)  )
loss_c1 = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=pred_c1)  )
loss_c2 = tf.reduce_mean( tf.nn.softmax_cross_entropy_with_logits(labels=y,logits=pred_c2)  )

cost = loss_full + (0.3 * loss_c1) + (0.3 * loss_c2)

optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

pred_full = tf.nn.softmax(pred_full)
#pred_full = .3* tf.nn.softmax(pred_c1) + .3* tf.nn.softmax(pred_c2) +  tf.nn.softmax(pred_full)


testPrediction = tf.argmax(pred_full, 1, name="prediction")

correct = tf.equal(tf.argmax(pred_full,1),tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct,tf.float64))

#correct_test = tf.equal(tf.argmax(test_prediction,1),tf.argmax(y,1))
#accuracy_test = tf.reduce_mean(tf.cast(correct_test,tf.float64))

tf.global_variables_initializer().run()

# Add ops to save and restore all the variables.
saver = tf.train.Saver()

classes = label_train - 1
rnn_data_train = getRNNFormat(ts_train, n_timestamps)
train_y = getLabelFormat(classes)
print np.unique(classes)

classes_test = label_test - 1
rnn_data_test = getRNNFormat(ts_test, n_timestamps)
test_y = getLabelFormat(classes_test)
print np.unique(classes_test)



iterations = rnn_data_train.shape[0] / batchsz

if rnn_data_train.shape[0] % batchsz != 0:
    iterations+=1

best_loss = sys.float_info.max


for e in range(hm_epochs):
	lossi = 0
	accS = 0
	
	rnn_data_train, vhsr_train, train_y = shuffle(rnn_data_train, vhsr_train, train_y, random_state=0)
	print "shuffle DONE"
	
	
	for ibatch in range(iterations):
	#for ibatch in range(10):
		#BATCH_X BATCH_Y: i-th batches of train_indices_x and train_y
		batch_rnn_x, _ = getBatch(rnn_data_train, train_y, ibatch, batchsz)
		
		#BATCH_X BATCH_Y: i-th batches of train_raw_x and train_y
		batch_cnn_x, batch_y = getBatch(vhsr_train, train_y, ibatch, batchsz)
		
		acc,_,loss = sess.run([accuracy,optimizer,cost],feed_dict={x_rnn:batch_rnn_x,
																	x_cnn:batch_cnn_x,
																	y:batch_y,
																	dropout_rnn:0.6,
																	learning_rate:0.0002})		
		lossi+=loss
		accS+=acc
		
		del batch_rnn_x
		del batch_cnn_x
		del batch_y
		
		
	print "Epoch:",e,"Train loss:",lossi/iterations,"| accuracy:",accS/iterations
	
	c_loss = lossi/iterations
	
	if c_loss < best_loss:
		if not os.path.exists("models"):
		        os.makedirs("models")
		save_path = saver.save(sess, "models/model_"+str(split_numb))
		print("Model saved in path: %s" % save_path)
		best_loss = c_loss
	'''	
	'''
	test_acc = checkTest(rnn_data_test, vhsr_test, 1024, classes_test)

	'''
	'''	
'''
'''

#np.save(outputFileName, np.array(tot_pred))

